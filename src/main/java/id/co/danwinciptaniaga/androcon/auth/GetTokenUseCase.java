package id.co.danwinciptaniaga.androcon.auth;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetTokenUseCase extends BaseObservable<GetTokenUseCase.Listener, TokenResponse> {

  private static final String TAG = GetTokenUseCase.class.getSimpleName();

  public interface Listener {
    void onGetTokenStarted();

    void onGetTokenSuccess(Resource<TokenResponse> result);

    void onGetTokenFailure(Resource<TokenResponse> result);
  }

  private final AuthorizationHeaderProvider ahp;
  private final AuthService authService;
  private final AppExecutors appExecutors;
  private final LoginUtil loginUtil;

  @Inject
  public GetTokenUseCase(Application application, AuthorizationHeaderProvider ahp,
      AuthService authService,
      AppExecutors appExecutors, LoginUtil loginUtil) {
    super(application);
    this.authService = authService;
    this.appExecutors = appExecutors;
    this.loginUtil = loginUtil;
    this.ahp = ahp;
  }

  public void getToken(String username, String password) {
    HyperLog.i(TAG, "getToken called");
    notifyStart(null, null);
    Call<TokenResponse> arCall = authService.getTokenCall(ahp.clientAuthorizationHeader(), username,
        password, "password");

    arCall.enqueue(new Callback<TokenResponse>() {
      @Override
      public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
        HyperLog.i(TAG, String.format("getToken onResponse: %s", response));
        appExecutors.networkIO().execute(
            () -> {
              ApiResponse<TokenResponse> apiResponse = ApiResponse.create(response);
              HyperLog.i(TAG, String.format("getToken apiResponse: %s", apiResponse));
              if (apiResponse instanceof ApiResponse.ApiSuccessResponse) {
                TokenResponse tokenResponse = ((ApiResponse.ApiSuccessResponse<TokenResponse>) apiResponse).getData();
                HyperLog.v(TAG, String.format("Obtained authentication token: %s",
                    tokenResponse.getAccessToken()));
                notifySuccess(null, tokenResponse);
              } else if (apiResponse instanceof ApiResponse.ApiErrorResponse) {

                notifyFailure(null,
                    ((ApiResponse.ApiErrorResponse) apiResponse).getErrorResponse(),
                    null);
              }
            });
      }

      @Override
      public void onFailure(Call<TokenResponse> call, Throwable t) {
        HyperLog.i(TAG, String.format("getToken onFailure: %s", t));
        appExecutors.networkIO().execute(
            () -> {
              notifyFailure(null, null, t);
            });
      }
    });
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<TokenResponse> result) {
    listener.onGetTokenStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<TokenResponse> result) {
    listener.onGetTokenSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<TokenResponse> result) {
    listener.onGetTokenFailure(result);
  }

}
