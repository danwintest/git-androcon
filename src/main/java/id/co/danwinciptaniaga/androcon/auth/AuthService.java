package id.co.danwinciptaniaga.androcon.auth;

import java.util.List;

import com.google.common.util.concurrent.ListenableFuture;

import androidx.lifecycle.LiveData;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AuthService {
  @POST("rest/v2/oauth/token")
  Call<TokenResponse> getTokenCall(@Header("Authorization") String authorization,
      @Query("username") String username, @Query("password") String password,
      @Query("grant_type") String grantType);

  @POST("rest/v2/oauth/token")
  LiveData<ApiResponse<TokenResponse>> loginLiveData(@Header("Authorization") String authorization,
      @Query("username") String username,
      @Query("password") String password, @Query("grant_type") String grantType);

  @POST("rest/v2/oauth/token")
  ListenableFuture<TokenResponse> refreshToken(@Header("Authorization") String authorization,
      @Query("refresh_token") String refresh_token, @Query("grant_type") String grantType);

  @POST("rest/v2/oauth/revoke")
  ListenableFuture<ResponseBody> revokeToken(@Header("Authorization") String authorization,
      @Query("token") String token, @Query("token_type_hint") String tokenTypeHint);

  @POST("rest/resetPassword")
  ListenableFuture<ResponseBody> resetPassword(@Header("Authorization") String authorization,
      @Query("username") String username);
}
