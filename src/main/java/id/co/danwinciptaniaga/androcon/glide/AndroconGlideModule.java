package id.co.danwinciptaniaga.androcon.glide;

import java.io.InputStream;
import java.util.UUID;

import javax.inject.Inject;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

import android.content.Context;
import androidx.annotation.NonNull;
import dagger.hilt.EntryPoint;
import dagger.hilt.InstallIn;
import dagger.hilt.android.EntryPointAccessors;
import dagger.hilt.android.components.ApplicationComponent;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;

@GlideModule
public class AndroconGlideModule extends AppGlideModule {

  @EntryPoint
  @InstallIn(ApplicationComponent.class)
  interface AndroconGlideEntryPoint {
    UtilityService getUtilityService();
  }

  @Override
  public void registerComponents(@NonNull Context context, @NonNull Glide glide,
      @NonNull Registry registry) {
    AndroconGlideEntryPoint ep = EntryPointAccessors
        .fromApplication(context.getApplicationContext(), AndroconGlideEntryPoint.class);
    AndroconModelLoaderFactory modelLoaderFactory =
        new AndroconModelLoaderFactory(ep.getUtilityService());
    registry.prepend(UUID.class, InputStream.class, modelLoaderFactory);
  }
}
