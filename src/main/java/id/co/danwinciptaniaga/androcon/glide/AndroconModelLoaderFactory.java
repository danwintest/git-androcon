package id.co.danwinciptaniaga.androcon.glide;

import java.io.InputStream;
import java.util.UUID;

import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;

import androidx.annotation.NonNull;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;

public class AndroconModelLoaderFactory implements ModelLoaderFactory<UUID, InputStream> {
  private UtilityService utilityService;

  public AndroconModelLoaderFactory(UtilityService utilityService) {
    this.utilityService = utilityService;
  }

  @NonNull
  @Override
  public ModelLoader<UUID, InputStream> build(@NonNull MultiModelLoaderFactory multiFactory) {
    return new AndroconModelLoader(utilityService);
  }

  @Override
  public void teardown() {

  }
}
