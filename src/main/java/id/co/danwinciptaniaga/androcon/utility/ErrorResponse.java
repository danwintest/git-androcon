package id.co.danwinciptaniaga.androcon.utility;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse {
  @SerializedName("error")
  private String error;

  @SerializedName("error_description")
  private String errorDescription;

  public String getError() {
    return error;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public void setError(String error) {
    this.error = error;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }

  public String getFormattedMessage() {
    return String.format("%s - %s", error, errorDescription);
  }

  @Override
  public String toString() {
    return new StringBuilder()
        .append("ErrorResponse{")
        .append("error='").append(error).append('\'')
        .append(", errorDescription='").append(errorDescription).append('\'')
        .append('}').toString();
  }
}

