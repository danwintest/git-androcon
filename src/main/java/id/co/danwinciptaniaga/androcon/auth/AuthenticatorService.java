package id.co.danwinciptaniaga.androcon.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public abstract class AuthenticatorService extends Service {
  private static final String TAG = AuthenticatorService.class.getSimpleName();

  protected static AccountAuthenticator sAccountAuthenticator;

  @Override
  public IBinder onBind(Intent intent) {
    Log.i(TAG, "onBind called: " + intent.getAction());
    IBinder binder = null;
    if (intent.getAction().equals(android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT)) {
      binder = getAuthenticator().getIBinder();
    }
    return binder;
  }

  protected abstract AccountAuthenticator getAuthenticator();
}
