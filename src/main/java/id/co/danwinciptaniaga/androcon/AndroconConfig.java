package id.co.danwinciptaniaga.androcon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;
import java.util.UUID;

import org.acra.ACRA;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.HttpSenderConfigurationBuilder;
import org.acra.data.StringFormat;
import org.acra.sender.HttpSender;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.common.util.concurrent.Futures;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hypertrack.hyperlog.HyperLog;
import com.hypertrack.hyperlog.LogFormat;
import com.scottyab.rootbeer.RootBeer;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.preference.PreferenceManager;
import id.co.danwinciptaniaga.androcon.registration.RegistrationUtil;
import id.co.danwinciptaniaga.androcon.update.UpdateUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * <p>Object ini perlu dipersiapkan per Aplikasi, berisi konfigurasi komponen spesifik untuk suatu Aplikasi.<br/>
 * Object ini digunakan oleh:
 * </p>
 * <li>
 *   <ul>WebServiceGenerator</ul>
 * </li>
 */
public class AndroconConfig {
  private static final String TAG = AndroconConfig.class.getSimpleName();
  public static final String STEP_INIT_HYPERLOG = "androconConfigInitializingHyperLog";
  public static final String STEP_INIT_ADID = "androconConfigInitializingAdid";
  public static final String STEP_INIT_DEVICE_INFO = "androconConfigInitializingDeviceInfo";
  public static final String STEP_INIT_FCM = "androconConfigInitializingFcm";
  public static final String STEP_INIT_ACRA = "androconConfigInitializingAcra";
  public static final String STEP_INIT_UPDATE = "androconConfigInitializingUpdate";
  public static final String STEP_ROOT_DETECTION_PROCESS = "androconConfigRootDetectionProcess";
  public static final String INTENT_INIT_PROGRESS = "androconConfigInitProgress";
  public static final String INTENT_INIT_COMPLETE = "androconConfigInitComplete";
  public static final String INTENT_ARG_STEP = "step";

  public static final String BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY = "SIMULATE_ANDROCON_INIT_DELAY";
  public static final int SIMULATE_INIT_DELAY_DURATION = 1000; // 1s

  private Application application;
  private boolean initialized = false;

  private Class buildConfigClass;
  private String deviceId;
  private String appVersionName;
  private long appVersionCode = -1;
  private String adid;
  private String apkFileName;

  // androcon server
  private String androconUrl;
  private String androconClientId;
  private String androconClientSecret;

  // retrofit
  private long retrofitHttpReadTimeout;
  private long retrofitHttpConnectTimeout;
  private HttpLoggingInterceptor.Level retrofitHttpLoggingLevel;

  // hyperlog
  private String hyperlogUrl = null;
  private LogFormat hyperlogFormat;
  private int hyperLogLevel = -1;

  // acra
  private CoreConfigurationBuilder acraCoreConfigurationBuilder;
  private String acraUrl = null;
  private String acraUsername;
  private String acraPassword;

  // fcm
  private String fcmToken;

  public AndroconConfig() {
  }

  private void setBuildConfigClass(Class buildConfigClass) {
    this.buildConfigClass = buildConfigClass;
  }

  private void setApkFileName(String apkFileName) {
    this.apkFileName = apkFileName;
  }

  private void setAndroconUrl(String androconUrl) {
    this.androconUrl = androconUrl;
  }

  private void setAndroconClientId(String androconClientId) {
    this.androconClientId = androconClientId;
  }

  private void setAndroconClientSecret(String androconClientSecret) {
    this.androconClientSecret = androconClientSecret;
  }

  private void setRetrofitHttpReadTimeout(long retrofitHttpReadTimeout) {
    this.retrofitHttpReadTimeout = retrofitHttpReadTimeout;
  }

  private void setRetrofitHttpConnectTimeout(long retrofitHttpConnectTimeout) {
    this.retrofitHttpConnectTimeout = retrofitHttpConnectTimeout;
  }

  private void setRetrofitHttpLoggingLevel(
      HttpLoggingInterceptor.Level retrofitHttpLoggingLevel) {
    this.retrofitHttpLoggingLevel = retrofitHttpLoggingLevel;
  }

  private void setHyperlogUrl(String hyperlogUrl) {
    this.hyperlogUrl = hyperlogUrl;
  }

  private void setHyperlogFormat(LogFormat hyperlogFormat) {
    this.hyperlogFormat = hyperlogFormat;
  }

  private void setHyperLogLevel(int hyperLogLevel) {
    this.hyperLogLevel = hyperLogLevel;
  }

  public void setAcraUrl(String acraUrl) {
    this.acraUrl = acraUrl;
  }

  public void setAcraUsername(String acraUsername) {
    this.acraUsername = acraUsername;
  }

  public void setAcraPassword(String acraPassword) {
    this.acraPassword = acraPassword;
  }

  public Class getBuildConfigClass() {
    return buildConfigClass;
  }

  public String getApkFileName() {
    return apkFileName;
  }

  public String getAppDeviceId() {
    if (adid == null) {
      adid = PreferenceManager.getDefaultSharedPreferences(application).getString(
          RegistrationUtil.SP_ADID, null);
    }
    return adid;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public String getAppVersionName() {
    return appVersionName;
  }

  public long getAppVersionCode() {
    return appVersionCode;
  }

  public String getAndroconUrl() {
    return androconUrl;
  }

  public String getAndroconClientId() {
    return androconClientId;
  }

  public String getAndroconClientSecret() {
    return androconClientSecret;
  }

  public long getRetrofitHttpReadTimeout() {
    return retrofitHttpReadTimeout;
  }

  public long getRetrofitHttpConnectTimeout() {
    return retrofitHttpConnectTimeout;
  }

  public HttpLoggingInterceptor.Level getRetrofitHttpLoggingLevel() {
    return retrofitHttpLoggingLevel;
  }

  public String getHyperlogUrl() {
    return hyperlogUrl;
  }

  public LogFormat getHyperlogFormat() {
    return hyperlogFormat;
  }

  public int getHyperLogLevel() {
    return hyperLogLevel;
  }

  public String getFcmToken() {
    if (fcmToken == null) {
      fcmToken = PreferenceManager.getDefaultSharedPreferences(application).getString(
          RegistrationUtil.SP_FCM_TOKEN, null);
    }
    return fcmToken;
  }

  public Boolean isRootDetected() {
    Boolean val = PreferenceManager.getDefaultSharedPreferences(application).getBoolean(
        Utility.SP_IS_ROOTED_DEVICE, false);
    return val;
  }

  public void init(Application application, AppExecutors executors) {
    this.application = application;
    Futures.submit(new Runnable() {
      @Override
      public void run() {
        long t1 = System.currentTimeMillis();
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(application);
        validateAndrocon();
        // 1. HyperLog: supaya logging dapat digunakan selanjutnya

        Intent i = new Intent(INTENT_INIT_PROGRESS);
        i.putExtra(INTENT_ARG_STEP, STEP_INIT_HYPERLOG);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        initHyperLog();

        // 2. Adid disiapkan seawal mungkin
        i.putExtra(INTENT_ARG_STEP, STEP_INIT_ADID);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        initAdid();

        // 3. Init info
        i.putExtra("step", STEP_INIT_DEVICE_INFO);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        initDeviceInfo();

        // 4. Init FCM -- async (dipaksa sync)
        i.putExtra("step", STEP_INIT_FCM);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        initFcm();

        // 5. init update related
        i.putExtra("step", STEP_INIT_UPDATE);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        initUpdate();

        // 6. init acra
        i.putExtra("step", STEP_INIT_ACRA);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        initAcra();

        i.putExtra("step", STEP_ROOT_DETECTION_PROCESS);
        lbm.sendBroadcast(i);
        Utility.simulateDelayIfBuildConfig(SIMULATE_INIT_DELAY_DURATION, buildConfigClass,
            AndroconConfig.BUILD_CONFIG_SIMULATE_ANDROCON_INIT_DELAY);
        rootDetectionProcess();

        long t2 = System.currentTimeMillis();
        long duration = t2 - t1;
        HyperLog.i(TAG, String.format("Androcon Initialized in %s ms", duration));
        setInitialized(true);
        i = new Intent(INTENT_INIT_COMPLETE);
        boolean acInitDoneHandled = lbm.sendBroadcast(i);
        HyperLog.i(TAG,
            INTENT_INIT_COMPLETE + " intent handled: " + acInitDoneHandled);
      }
    }, executors.backgroundIO());
  }

  synchronized private void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }

  synchronized public boolean isInitialized() {
    return this.initialized;
  }

  private void initDeviceInfo() {
    TelephonyManager tm = (TelephonyManager) application.getSystemService(
        Context.TELEPHONY_SERVICE);
    try {
      this.deviceId = tm.getDeviceId();
    } catch (Exception e) {
      HyperLog.w(TAG, String.format("Unable to obtain deviceId: %s", e.getMessage()));
    }

    try {
      PackageInfo pi = application.getPackageManager().getPackageInfo(application.getPackageName(),
          0);
      this.appVersionName = pi.versionName;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        this.appVersionCode = pi.getLongVersionCode();
      } else {
        this.appVersionCode = pi.versionCode;
      }
    } catch (Exception e) {
      HyperLog.w(TAG, "Unable to obtain appVersionName", e);
    }

  }

  private void initAcra() {
    acraCoreConfigurationBuilder = new CoreConfigurationBuilder(this.application);
    acraCoreConfigurationBuilder
        .setBuildConfigClass(buildConfigClass)
        .setReportFormat(StringFormat.KEY_VALUE_LIST)
        .setResReportSendSuccessToast(R.string.error_report_successful)
        .setResReportSendFailureToast(R.string.error_report_failed);
    acraCoreConfigurationBuilder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder.class)
        .setUri(acraUrl)
        .setBasicAuthLogin(acraUsername)
        .setBasicAuthPassword(acraPassword)
        .setHttpMethod(HttpSender.Method.POST)
        .setEnabled(true);
    ACRA.DEV_LOGGING = Utility.getBuildConfigAsBoolean(buildConfigClass, "DEBUG");
    ACRA.init(application, acraCoreConfigurationBuilder);
  }

  private void rootDetectionProcess(){
    HyperLog.d(TAG, "rootDetectionProcess[Start]");
    long t1 = System.currentTimeMillis();
    boolean isRootedDevice = false;
    RootBeer rootBeer = new RootBeer(this.application);
    if (rootBeer.isRooted()) {
      HyperLog.d(TAG, "we found indication of root");
      isRootedDevice = true;
    } else {
      HyperLog.d(TAG, "we didn't find indication of root");
      isRootedDevice = false;
    }
    PreferenceManager.getDefaultSharedPreferences(this.application).edit()
        .putBoolean(Utility.SP_IS_ROOTED_DEVICE, isRootedDevice)
        .commit();

    long t2 = System.currentTimeMillis();
    HyperLog.d(TAG, "rootDetectionProcess[Finish] " + (t2 - t1) + " ms");
  }


  private void initAdid() {
    // menurut dokumentasi, Android Q tidak lagi mengizinkan akses langsung ke dir di luar package
    // jadi, dir yang digunakan adalah yg di dalam package (untuk semua versi Android)
    File dir = this.application.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
    File didFile = new File(dir, "did");

    boolean validAdid = false;

    if (didFile.exists()) {
      BufferedReader br = null;
      String line = null;
      try {
        br = new BufferedReader(new FileReader(didFile));
        line = br.readLine();
      } catch (Exception e) {
        HyperLog.w(TAG, "ADID file exists, but cannot be read", e);
      } finally {
        try {
          br.close();
        } catch (IOException e) {
        }
      }
      if (!TextUtils.isEmpty(line)) {
        this.adid = line;
        validAdid = true;
        // berhasil mendapatkan ADID
      }
    }
    // kalau tidak dapat valid ADID, maka buat baru
    if (!validAdid) {
      PrintWriter pw = null;
      try {
        pw = new PrintWriter(new FileWriter(didFile));
        adid = UUID.randomUUID().toString();
        HyperLog.i(TAG, "ADID is " + adid);
        pw.print(adid);
        pw.flush();

        // simpan di SP
        this.adid = adid;
        PreferenceManager.getDefaultSharedPreferences(this.application).edit()
            .putString(RegistrationUtil.SP_ADID, adid)
            .putBoolean(RegistrationUtil.SP_ADID_REGISTERED, false)
            .commit();
      } catch (Exception e) {
        throw new RuntimeException("Unable to initialize ADID", e);
      } finally {
        pw.close();
      }
    } else {
      String existingAdid = PreferenceManager.getDefaultSharedPreferences(
          this.application).getString(
          RegistrationUtil.SP_ADID, null);
      if (!adid.toString().equals(existingAdid)) {
        HyperLog.i(TAG, "Replacing existing ADID " + existingAdid + " with " + adid);
        this.adid = adid;
        PreferenceManager.getDefaultSharedPreferences(this.application)
            .edit()
            .putString(RegistrationUtil.SP_ADID, adid)
            .putBoolean(RegistrationUtil.SP_ADID_REGISTERED, false)
            .commit();
      } else {
        HyperLog.i(TAG, "ADID " + adid + " is already set");
      }
    }
  }

  private void initFcm() {
    long t1 = System.currentTimeMillis();
    // dengan ACRA aktif, FirebaseApp harus diinisialisasi di sini supaya tidak error
    FirebaseApp.initializeApp(application);
    Task<String> getTokenTask = FirebaseMessaging.getInstance().getToken();
    try {
      fcmToken = Tasks.await(getTokenTask);
      // Log and toast
      String msg = "FCM token obtained: " + fcmToken;
      Log.d(TAG, msg);
      PreferenceManager.getDefaultSharedPreferences(AndroconConfig.this.application).edit()
          .putString(RegistrationUtil.SP_FCM_TOKEN, fcmToken)
          .putString(RegistrationUtil.SP_FCM_STATUS, RegistrationUtil.SP_FCM_STATUS_READY)
          .commit();
    } catch (Exception e) {
      Log.w(TAG, "Fetching FCM registration token failed", e);
      PreferenceManager.getDefaultSharedPreferences(AndroconConfig.this.application).edit()
          .putString(RegistrationUtil.SP_FCM_TOKEN, fcmToken)
          .putString(RegistrationUtil.SP_FCM_STATUS, RegistrationUtil.SP_FCM_STATUS_ERROR)
          .commit();
    }
    long t2 = System.currentTimeMillis();
    HyperLog.d(TAG, "Fetch FCM token took " + (t2 - t1) + " ms");
  }

  private void validateAndrocon() {
    if (TextUtils.isEmpty(androconUrl))
      throw new IllegalArgumentException("Android Connector URL must be set");
    if (TextUtils.isEmpty(androconClientId))
      throw new IllegalArgumentException("Android Connector Client ID must be set");
    if (TextUtils.isEmpty(androconClientSecret))
      throw new IllegalArgumentException("Android Connector Client Secret must be set");
  }

  private void initHyperLog() {
    HyperLog.initialize(this.application);
    HyperLog.setURL(this.hyperlogUrl != null ? this.hyperlogUrl : this.androconUrl);
    if (hyperlogFormat != null) {
      HyperLog.setLogFormat(this.hyperlogFormat);
    }
    if (hyperLogLevel != -1) {
      HyperLog.setLogLevel(hyperLogLevel);
    }
  }

  private void initUpdate() {
    PackageInfo pi = null;
    try {
      pi = application.getPackageManager().getPackageInfo(application.getPackageName(), 0);
    } catch (PackageManager.NameNotFoundException e) {
      HyperLog.w(TAG, "Problem reading PackageInfo", e);
    }
    if (pi != null) {
      String savedMarker = UpdateUtility.getVersionMarker(application);
      String currentMarker = UpdateUtility.generateVersionMarker(pi);
      HyperLog.i(TAG,
          String.format("Saved vs current version marker = %s vs %s", savedMarker, currentMarker));

      int installerStatus = UpdateUtility.getInstallerStatus(application);
      if (!Objects.equals(savedMarker, currentMarker)) {
        // kalau marker beda (app sudah diupdate), maka:
        // - update marker
        // - hapus installer
        UpdateUtility.setVersionMarker(application, currentMarker);
        // clear status installer
        UpdateUtility.setInstallerStatus(application, UpdateUtility.INSTALLER_STATUS_NONE);
        UpdateUtility.clearDownloadId(application);
        File updateFile = UpdateUtility.getUpdateFilePath(application, apkFileName);
        if (updateFile.exists()) {
          HyperLog.i(TAG, "Update applied, deleting update file: " + updateFile.getAbsolutePath());
          updateFile.delete();
          UpdateUtility.clearDownloadId(application);
        } else {
          HyperLog.w(TAG, "Update applied, but Update file does not exist");
        }
      } else {
        // kalau marker sama (app belum diupdate), maka:
        // - kalau status READY, trigger update;
        //   else tidak ada apa-apa
        if (installerStatus == UpdateUtility.INSTALLER_STATUS_READY) {
          // status ready, trigger update
          UpdateUtility.triggerUpdate(application, apkFileName);
        }
      }
    } else {
      HyperLog.w(TAG, "Unable to check for update installer status");
    }
  }

  public static class Builder {
    private Class buildConfigClass;
    private String apkFileName;

    // androcon server
    private String androconUrl;
    private String androconClientId;
    private String androconClientSecret;

    // retrofit
    private long retrofitHttpReadTimeout = 60;
    private long retrofitHttpConnectTimeout = 60;
    private HttpLoggingInterceptor.Level retrofitHttpLoggingLevel = HttpLoggingInterceptor.Level.HEADERS;

    // hyperlog
    private String hyperlogUrl = null;
    private LogFormat hyperlogFormat;
    private int hyperLogLevel = -1;

    // acra
    private String acraUrl = null;
    private String acraUsername;
    private String acraPassword;

    public Builder() {
    }

    public Builder setBuildConfigClass(Class clazz) {
      this.buildConfigClass = clazz;
      return this;
    }

    public Builder setAndroconUrl(String androconUrl) {
      this.androconUrl = androconUrl;
      return this;
    }

    public Builder setClientId(String clientId) {
      this.androconClientId = clientId;
      return this;
    }

    public Builder setClientSecret(String clientSecret) {
      this.androconClientSecret = clientSecret;
      return this;
    }

    public Builder setRetrofitHttpReadTimeout(long retrofitHttpReadTimeout) {
      this.retrofitHttpReadTimeout = retrofitHttpReadTimeout;
      return this;
    }

    public Builder setRetrofitHttpConnectTimeout(long retrofitHttpConnectTimeout) {
      this.retrofitHttpConnectTimeout = retrofitHttpConnectTimeout;
      return this;
    }

    public Builder setRetrofitHttpLoggingLevel(
        HttpLoggingInterceptor.Level retrofitHttpLoggingLevel) {
      this.retrofitHttpLoggingLevel = retrofitHttpLoggingLevel;
      return this;
    }

    public Builder setHyperlogUrl(String hyperlogUrl) {
      this.hyperlogUrl = hyperlogUrl;
      return this;
    }

    public Builder setHyperlogFormat(LogFormat hyperlogFormat) {
      this.hyperlogFormat = hyperlogFormat;
      return this;
    }

    public Builder setHyperLogLevel(int hyperLogLevel) {
      this.hyperLogLevel = hyperLogLevel;
      return this;
    }

    public Builder setAcraUrl(String acraUrl) {
      this.acraUrl = acraUrl;
      return this;
    }

    public Builder setAcraUsername(String acraUsername) {
      this.acraUsername = acraUsername;
      return this;
    }

    public Builder setAcraPassword(String acraPassword) {
      this.acraPassword = acraPassword;
      return this;
    }

    public Builder setApkFileName(String apkFileName) {
      this.apkFileName = apkFileName;
      return this;
    }

    /**
     * Method ini hanya mempersiapkan konfigurasi awal yang diperlukan pada AndroconConfig
     *
     * @return
     */
    public AndroconConfig build() {
      AndroconConfig config = new AndroconConfig();
      config.setBuildConfigClass(buildConfigClass);
      config.setApkFileName(apkFileName);
      config.setAndroconUrl(this.androconUrl);
      config.setAndroconClientId(this.androconClientId);
      config.setAndroconClientSecret(this.androconClientSecret);

      config.setRetrofitHttpConnectTimeout(this.retrofitHttpConnectTimeout);
      config.setRetrofitHttpReadTimeout(this.retrofitHttpReadTimeout);
      config.setRetrofitHttpLoggingLevel(this.retrofitHttpLoggingLevel);

      config.setHyperlogUrl(this.hyperlogUrl);
      config.setHyperLogLevel(this.hyperLogLevel);
      config.setHyperlogFormat(this.hyperlogFormat);

      config.setAcraUrl(acraUrl);
      config.setAcraUsername(acraUsername);
      config.setAcraPassword(acraPassword);

      return config;
    }
  }
}
