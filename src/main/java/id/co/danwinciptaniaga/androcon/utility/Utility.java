package id.co.danwinciptaniaga.androcon.utility;

import java.lang.reflect.Field;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import androidx.preference.PreferenceManager;

public class Utility {
  private static final String TAG = Utility.class.getSimpleName();
  public static final String SP_IS_ROOTED_DEVICE = "isRootedDevice";
  public static final String SP_BYPASS_ROOT_DETECTION = "byPassRootDetection";

  public static String getMetadata(Context context, String key) {
    try {
      ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(),
          PackageManager.GET_META_DATA);
      if (ai.metaData != null) {
        return ai.metaData.getString(key);
      }
    } catch (PackageManager.NameNotFoundException e) {
      // if we can't find it in the manifest, just return null
    }
    return null;
  }

  public static boolean isDebug(Class buildConfigClass) {
    return getBuildConfigAsBoolean(buildConfigClass, "DEBUG");
  }

  public static boolean getBuildConfigAsBoolean(Class buildConfigClass, String fieldName) {
    if (buildConfigClass == null)
      throw new IllegalArgumentException("BuildConfig class must be set");
    if (!"BuildConfig".equals(buildConfigClass.getSimpleName()))
      throw new IllegalArgumentException("Invalid BuildConfig class name");

    try {
      Field debugField = buildConfigClass.getField(fieldName);
      return debugField.getBoolean(null);
    } catch (Exception e) {
      HyperLog.exception(TAG, e);
      return false;
    }
  }

  public static String getTrimmedString(Editable e) {
    if (e.length() == 0) {
      return null;
    } else {
      String s = e.toString().trim();
      if (s.length() == 0) {
        return null;
      } else {
        return s;
      }
    }
  }

  public static String printBundle(Bundle bundle) {
    StringBuilder result = new StringBuilder();
    if (bundle != null) {
      for (String k : bundle.keySet()) {
        result.append(String.format("\t%s = %s\n", k, bundle.get(k)));
      }
    } else {
      result.append("null");
    }
    return result.toString();
  }

  public static void simulateDelayIfBuildConfig(long ms, Class buildConfigClass,
      String configField) {
    if (getBuildConfigAsBoolean(buildConfigClass, configField)) {
      try {
        Thread.sleep(ms);
      } catch (InterruptedException e) {
      }
    }
  }

  public static Boolean isRootDetected(Context context) {
    Boolean val = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
        Utility.SP_IS_ROOTED_DEVICE, false);
    return val;
  }

  public static void setByPassRootDetection(Context context, boolean byPassRootDetection) {
    PreferenceManager.getDefaultSharedPreferences(context)
        .edit()
        .putBoolean(SP_BYPASS_ROOT_DETECTION, byPassRootDetection)
        .commit();
  }

  public static boolean byPassRootDetection(Context context) {
    return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
        SP_BYPASS_ROOT_DETECTION, false);
  }
}