package id.co.danwinciptaniaga.androcon.retrofit;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import android.text.TextUtils;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import retrofit2.Response;

public class ApiResponse<T> {

  private static final String TAG = ApiResponse.class.getSimpleName();

  public static <T> ApiExceptionResponse create(Throwable t) {
    return new ApiExceptionResponse(null, t);
  }

  /**
   * Method ini menerjemahkan Response dari Retrofit menjadi ApiResponse yang sudah diklasifikasi.
   * <ul>
   *   <li>Code 200-299:
   *   <ul>
   *     <li>Body null atau Code 204: {@link ApiEmptyResponse}</li>
   *     <li>Selain itu: {@link ApiSuccessResponse}</li>
   *   </ul>
   *   </li>
   *   <li>Selain itu:
   *     <ul>
   *       <li>coba baca ErrorBody dan parse, kalau berhasil {@link ApiErrorResponse}</li>
   *       <li>kalau gagal, coba baca Exception messsage</li>
   *     </ul>
   *   </li>
   * </ul>
   *
   * @param response
   * @param <T>
   * @return
   */
  public static <T> ApiResponse<T> create(Response<T> response) {
    ApiResponse<T> result = null;
    if (response.isSuccessful()) {
      T body = response.body();
      if (body == null || response.code() == 204) {
        result = new ApiEmptyResponse();
      } else {
        result = new ApiSuccessResponse(body);
      }
    } else {
      if (response.errorBody() != null) {
        ErrorResponse errorResponse = RetrofitUtility.parseErrorBody(response.errorBody());
        result = new ApiErrorResponse<T>(null, errorResponse);
      }

      if (result == null) {
        String msg = response.errorBody() != null ? response.errorBody().toString() : null;
        String errorMsg = null;
        if (TextUtils.isEmpty(msg)) {
          errorMsg = response.message();
        } else {
          errorMsg = msg;
        }
        result = create(new Exception(errorMsg != null ? errorMsg : "unknown error"));
      }
    }
    return result;
  }

  public static class ApiEmptyResponse<T> extends ApiResponse<T> {
  }

  public static class ApiSuccessResponse<T> extends ApiResponse<T> {
    private T data;

    public ApiSuccessResponse(T data) {
      this.data = data;
    }

    public T getData() {
      return data;
    }
  }

  public static class ApiErrorResponse<T> extends ApiResponse<T> {
    private T data;
    private ErrorResponse errorResponse;

    public ApiErrorResponse(T data, ErrorResponse errorResponse) {
      this.data = data;
      this.errorResponse = errorResponse;
    }

    public T getData() {
      return data;
    }

    public ErrorResponse getErrorResponse() {
      return errorResponse;
    }
  }

  public static class ApiExceptionResponse<T, E extends Throwable> extends ApiErrorResponse<T> {
    private E throwable;

    public ApiExceptionResponse(T data, E throwable) {
      super(data, null);
      this.throwable = throwable;
    }

    public E getThrowable() {
      return throwable;
    }
  }
}
