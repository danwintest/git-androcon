package id.co.danwinciptaniaga.androcon.auth;

import com.google.gson.annotations.SerializedName;

public class PermissionInfo {

  @SerializedName("intValue")
  private int intValue;

  @SerializedName("type")
  private String type;

  @SerializedName("value")
  private String value;

  @SerializedName("target")
  private String target;

  public int getIntValue() {
    return intValue;
  }

  public String getType() {
    return type;
  }

  public String getValue() {
    return value;
  }

  public String getTarget() {
    return target;
  }
}