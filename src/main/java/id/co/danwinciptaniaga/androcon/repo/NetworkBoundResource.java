package id.co.danwinciptaniaga.androcon.repo;

import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public abstract class NetworkBoundResource<ResultType, RequestType> {
  private AppExecutors appExecutors;
  private MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

  @MainThread
  public NetworkBoundResource(AppExecutors appExecutors) {
    this.appExecutors = appExecutors;
    result.setValue(Resource.Builder.loading(null, null));
    //    @Suppress("LeakingThis")
    LiveData<ResultType> dbSource = loadFromDb();
    result.addSource(dbSource, data -> {
      result.removeSource(dbSource);
      if (shouldFetch(data)) {
        fetchFromNetwork(dbSource);
      } else {
        result.addSource(dbSource,
            newData -> NetworkBoundResource.this.setValue(Resource.Builder.success(null, newData)));
      }
    });
  }

  @MainThread
  private void setValue(Resource<ResultType> newValue) {
    if (result.getValue() != newValue) {
      result.setValue(newValue);
    }
  }

  private void fetchFromNetwork(LiveData<ResultType> dbSource) {
    LiveData<ApiResponse<RequestType>> apiResponse = createCall();
    // we re-attach dbSource as a new source, it will dispatch its latest value quickly
    result.addSource(dbSource,
        newData ->
            setValue(Resource.Builder.loading(null, newData))
    );
    result.addSource(apiResponse,
        response -> {
          result.removeSource(apiResponse);
          result.removeSource(dbSource);

          if (response instanceof ApiResponse.ApiSuccessResponse) {
            appExecutors.diskIO().execute(() -> {
              saveCallResult(
                  processResponse((ApiResponse.ApiSuccessResponse<RequestType>) response));
              appExecutors.mainThread().execute(() -> {
                // we specially request a new live data,
                // otherwise we will get immediately last cached value,
                // which may not be updated with latest results received from network.
                result.addSource(loadFromDb(),
                    newData -> {
                      setValue(Resource.Builder.success(null, newData));
                    });
              });
            });
          } else if (response instanceof ApiResponse.ApiEmptyResponse) {
            appExecutors.mainThread().execute(() -> {
              // reload from disk whatever we had
              result.addSource(loadFromDb(), newData -> {
                setValue(Resource.Builder.success(null, newData));
              });
            });
          } else if (response instanceof ApiResponse.ApiExceptionResponse) {
            onFetchFailed();
            result.addSource(dbSource, newData -> {
              setValue(Resource.Builder.error(null, newData));
            });
          }
        });
  }

  protected void onFetchFailed() {
  }

  public LiveData<Resource<ResultType>> asLiveData() {
    return result;
  }

  @WorkerThread
  protected RequestType processResponse(ApiResponse.ApiSuccessResponse<RequestType> response) {
    return response.getData();
  }

  @WorkerThread
  protected abstract void saveCallResult(RequestType item);

  /**
   * Mengembalikan <code>ResultType</code> dari DB lokal
   *
   * @param data
   * @return
   */
  @MainThread
  protected abstract Boolean shouldFetch(ResultType data);

  /**
   * Implementasi ini mengembalikan <code>LiveData<ResultType></code> dari DB lokal.
   *
   * @return
   */
  @MainThread
  protected abstract LiveData<ResultType> loadFromDb();

  @MainThread
  protected abstract LiveData<ApiResponse<RequestType>> createCall();
}
