package id.co.danwinciptaniaga.androcon.security;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public abstract class ProtectedFragment extends Fragment {
  protected ProtectedActivityViewModel basemodel;

  @Override
  @CallSuper
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    basemodel = new ViewModelProvider(requireActivity()).get(ProtectedActivityViewModel.class);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    basemodel.getAuthenticationDone().observe(getViewLifecycleOwner(),
        this::authenticationStatusUpdate);
  }

  @UiThread
  protected abstract void authenticationStatusUpdate(Resource<String> status);
}
