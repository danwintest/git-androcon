package id.co.danwinciptaniaga.androcon.retrofit;

import java.io.IOException;

import id.co.danwinciptaniaga.androcon.AndroconConfig;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class InfoInterceptor implements Interceptor {

  private final AndroconConfig androconConfig;

  public InfoInterceptor(AndroconConfig androconConfig) {
    this.androconConfig = androconConfig;
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    Request original = chain.request();
    Request.Builder builder = original.newBuilder();

    String uuid = androconConfig.getAppDeviceId();
    if (uuid != null) {
      builder.header("ADID", uuid);
    }
    if (androconConfig.getAppVersionCode() != -1) {
      builder.header("VersionCode", String.valueOf(androconConfig.getAppVersionCode()));
    }
    if (androconConfig.getAppVersionName() != null) {
      builder.header("VersionName", androconConfig.getAppVersionName());
    }

    if (androconConfig.getDeviceId() != null) {
      builder.header("DID", androconConfig.getDeviceId());
    }

    Request request = builder.build();
    return chain.proceed(request);
  }
}
