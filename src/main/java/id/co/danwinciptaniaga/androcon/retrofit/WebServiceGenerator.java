package id.co.danwinciptaniaga.androcon.retrofit;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.guava.GuavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Singleton
public class WebServiceGenerator {
  private final Application mApplication;
  private final LoginUtil mLoginUtil;
  private final AndroconConfig mAndroconConfig;

  protected OkHttpClient.Builder okHttpClientBuilder = null;

  protected HttpLoggingInterceptor httpLoggingInterceptor = null;
  protected SecurityInterceptor securityInterceptor = null;
  protected InfoInterceptor infoInterceptor = null;

  protected Retrofit.Builder builder = null;

  @Inject
  public WebServiceGenerator(Application application, AndroconConfig androconConfig,
      LoginUtil loginUtil, OkHttpClient.Builder okHttpClientBuilder) {
    this.mApplication = application;
    this.mAndroconConfig = androconConfig;
    this.mLoginUtil = loginUtil;
    this.okHttpClientBuilder = okHttpClientBuilder;
  }

  public Retrofit.Builder getBuilder() {
    if (builder == null) {
      ObjectMapper om = new ObjectMapper();
      om.registerModule(new JavaTimeModuleDW());
      om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      builder = new Retrofit.Builder()
          .baseUrl(mAndroconConfig.getAndroconUrl())
          .addConverterFactory(ScalarsConverterFactory.create())
          .addConverterFactory(JacksonConverterFactory.create(om))
          .addConverterFactory(new JacksonStringConverterFactory(om))
          .addCallAdapterFactory(GuavaCallAdapterFactory.create());

    }
    return builder;
  }

  public <S> S createService(Class<S> serviceClass) {
    boolean changed = false;
    Retrofit retrofit = null;
    // setup Security Interceptor
    SecurityInterceptor securityInterceptor = getSecurityInterceptor();
    if (!okHttpClientBuilder.interceptors().contains(securityInterceptor)) {
      okHttpClientBuilder.addInterceptor(securityInterceptor);
      changed = true;
    }

    // setup InfoInterceptor
    InfoInterceptor infoInterceptor = getInfoInterceptor();
    if (!okHttpClientBuilder.interceptors().contains(infoInterceptor)) {
      okHttpClientBuilder.addInterceptor(infoInterceptor);
      changed = true;
    }

    if (Utility.isDebug(mAndroconConfig.getBuildConfigClass())) {
      HttpLoggingInterceptor logging = getHttpLoggingInterceptor();
      logging.setLevel(mAndroconConfig.getRetrofitHttpLoggingLevel());

      if (!okHttpClientBuilder.interceptors().contains(logging)) {
        okHttpClientBuilder.addInterceptor(logging);
        okHttpClientBuilder.followRedirects(false);
        changed = true;
      }
    }
    if (changed) {
      getBuilder().client(okHttpClientBuilder.build());
    }
    retrofit = getBuilder().build();

    return retrofit.create(serviceClass);
  }

  /**
   * Method lazy-load untuk {@link SecurityInterceptor}
   *
   * @return
   */
  private SecurityInterceptor getSecurityInterceptor() {
    if (securityInterceptor == null) {
      securityInterceptor = new SecurityInterceptor(mApplication, mAndroconConfig, mLoginUtil);
    }
    return securityInterceptor;
  }

  /**
   * Method lazy-load untuk {@link InfoInterceptor}
   *
   * @return
   */
  private InfoInterceptor getInfoInterceptor() {
    if (infoInterceptor == null) {
      infoInterceptor = new InfoInterceptor(mAndroconConfig);
    }
    return infoInterceptor;
  }

  /**
   * Method lazy-load untuk {@link HttpLoggingInterceptor}
   *
   * @return
   */
  private HttpLoggingInterceptor getHttpLoggingInterceptor() {
    if (httpLoggingInterceptor == null) {
      httpLoggingInterceptor = new HttpLoggingInterceptor();
    }
    return httpLoggingInterceptor;
  }
}
