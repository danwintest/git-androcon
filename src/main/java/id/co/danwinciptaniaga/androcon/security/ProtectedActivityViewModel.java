package id.co.danwinciptaniaga.androcon.security;

import java.util.List;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.auth.AuthService;
import id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.GetUserPermissionUseCase;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.auth.TokenResponse;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import okhttp3.ResponseBody;

public class ProtectedActivityViewModel extends AndroidViewModel
    implements GetUserPermissionUseCase.Listener {
  private static final String TAG = ProtectedActivityViewModel.class.getSimpleName();

  private final GetUserPermissionUseCase ucGetUserPermission;
  private final AppExecutors appExecutors;
  private final LoginUtil loginUtil;
  private final AuthService authService;
  private final AuthorizationHeaderProvider ahp;

  private MutableLiveData<Resource<String>> authenticationStatus = new MutableLiveData<>();
  private final MutableLiveData<Resource<List<PermissionInfo>>> permissionsLd = new MutableLiveData<>(
      null);

  @ViewModelInject
  public ProtectedActivityViewModel(@NonNull Application application,
      GetUserPermissionUseCase getUserPermissionUseCase, LoginUtil loginUtil,
      AuthService authService, AuthorizationHeaderProvider ahp, AppExecutors appExecutors) {
    super(application);
    this.ucGetUserPermission = getUserPermissionUseCase;
    this.ucGetUserPermission.registerListener(this);
    this.loginUtil = loginUtil;
    this.authService = authService;
    this.ahp = ahp;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetUserPermission.unregisterListener(this);
  }

  public LiveData<Resource<String>> getAuthenticationDone() {
    return authenticationStatus;
  }

  public void setAuthenticationStatus(Resource<String> status) {
    this.authenticationStatus.postValue(status);
  }

  public LiveData<Resource<List<PermissionInfo>>> getPermissionsLd() {
    return permissionsLd;
  }

  public void triggerGetPermissions() {
    ucGetUserPermission.getUserPermission();
  }

  @Override
  public void onGetUserPermissionStarted() {
  }

  @Override
  public void onGetUserPermissionSuccess(Resource<List<PermissionInfo>> result) {
    permissionsLd.postValue(result);
  }

  @Override
  public void onGetUserPermissionFailure(Resource<List<PermissionInfo>> result) {
    permissionsLd.postValue(result);
  }

  public void logout() {
    String token = loginUtil.getSavedToken();
    if (token != null) {
      HyperLog.i(TAG,
          "Logging out and spToken exists, attempting to revoke it on server: " + token);
      ListenableFuture<ResponseBody> lf = authService
          .revokeToken(ahp.clientAuthorizationHeader(), token, null);
      Futures.addCallback(lf, new FutureCallback<ResponseBody>() {
        @Override
        public void onSuccess(@NullableDecl ResponseBody result) {
          HyperLog.i(TAG, "Revoke token response: " + result);
        }

        @Override
        public void onFailure(Throwable t) {
          HyperLog.w(TAG, "Failed to revoke token response", t);
        }
      }, appExecutors.networkIO());
    } else {
      HyperLog.i(TAG,
          "Logging out but spToken does not exists, NOT attempting to revoke token on server");
    }
    // tetap logout
    loginUtil.logout();
  }
}
