package id.co.danwinciptaniaga.androcon.registration;

import javax.inject.Inject;
import javax.inject.Singleton;

import android.content.Context;
import androidx.preference.PreferenceManager;
import dagger.hilt.android.qualifiers.ApplicationContext;
import id.co.danwinciptaniaga.androcon.utility.Utility;

@Singleton
public class RegistrationUtil {
  public static final String SP_ADID = "ADID";
  public static final String SP_ADID_REGISTERED = "ADID_Registered";
  public static final String SP_FCM_TOKEN = "fcmToken";
  public static final String SP_FCM_STATUS = "fcmTokenStatus";

  public static final String SP_FCM_STATUS_NOT_INITIALIZED = "not_initialized";
  public static final String SP_FCM_STATUS_READY = "ready";
  public static final String SP_FCM_STATUS_ERROR = "error";

  private Context context;

  @Inject
  public RegistrationUtil(@ApplicationContext Context context) {
    this.context = context;
  }

  public boolean isRegisteredToServer() {
    return isRegisteredToServer(this.context);
  }

  public void setRegisteredToServer(boolean registered) {
    setRegisteredToServer(this.context, registered);
  }

  public void setByPassRootDetection(boolean byPassRootDetection) {
    Utility.setByPassRootDetection(this.context, byPassRootDetection);
  }

  public static boolean isRegisteredToServer(Context context) {
    return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SP_ADID_REGISTERED,
        false);
  }

  public static void setRegisteredToServer(Context context, boolean registered) {
    PreferenceManager.getDefaultSharedPreferences(context)
        .edit()
        .putBoolean(SP_ADID_REGISTERED, registered)
        .commit();
  }
}
