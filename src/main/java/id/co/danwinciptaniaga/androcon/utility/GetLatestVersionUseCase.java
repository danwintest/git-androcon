package id.co.danwinciptaniaga.androcon.utility;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.acs.data.LatestAppVersionResponse;
import id.co.danwinciptaniaga.androcon.R;

public class GetLatestVersionUseCase
    extends BaseObservable<GetLatestVersionUseCase.Listener, LatestAppVersionResponse> {
  public interface Listener {

    void onGetLatestVersionStarted(Resource<LatestAppVersionResponse> loading);

    void onGetLatestVersionSuccess(Resource<LatestAppVersionResponse> response);

    void onGetLatestVersionFailure(Resource<LatestAppVersionResponse> response);

  }

  private final UtilityService utilityService;
  private final AppExecutors appExecutors;

  @Inject
  public GetLatestVersionUseCase(Application application, AppExecutors appExecutors,
      UtilityService utilityService) {
    super(application);
    this.utilityService = utilityService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<LatestAppVersionResponse> getLatestVersion() {
    notifyStart(application.getString(R.string.msg_checking_new_version), null);
    ListenableFuture<LatestAppVersionResponse> getLatestVersionLf = utilityService.getLatestVersion();
    Futures.addCallback(getLatestVersionLf, new FutureCallback<LatestAppVersionResponse>() {
      @Override
      public void onSuccess(@NullableDecl LatestAppVersionResponse result) {
        notifySuccess(application.getString(R.string.msg_check_new_version_success), result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure(application.getString(R.string.msg_check_new_version_failed), null, t);
      }
    }, appExecutors.backgroundIO());
    return getLatestVersionLf;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<LatestAppVersionResponse> result) {
    listener.onGetLatestVersionStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<LatestAppVersionResponse> result) {
    listener.onGetLatestVersionSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<LatestAppVersionResponse> result) {
    listener.onGetLatestVersionFailure(result);
  }
}
