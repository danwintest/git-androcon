package id.co.danwinciptaniaga.androcon.auth;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.os.Bundle;

public class AccountManagerUtil {
  @NotNull
  public static ListenableFuture<Bundle> getAuthTokenLf(AccountManager am, Account account,
      String authTokenType, Bundle options, Executor executor) {
    return Futures.submit(new Callable<Bundle>() {
      @Override
      public Bundle call() throws Exception {
        AccountManagerFuture<Bundle> authTokenF = am.getAuthToken(account, authTokenType, options,
            true, null, null);
        return authTokenF.getResult();
      }
    }, executor);
  }

  public static void setAccountAuthToken(AccountManager am, Account account, String authTokenType,
      String authToken, String refreshToken, long tokenAcquiredAt, int expiresIn) {
    am.setAuthToken(account, authTokenType, authToken);
    am.setUserData(account, AccountAuthenticator.USERDATA_REFRESH_TOKEN, refreshToken);
    am.setUserData(account, AccountAuthenticator.USERDATA_TOKEN_ACQUIRED_AT,
        String.valueOf(tokenAcquiredAt));
    am.setUserData(account, AccountAuthenticator.USERDATA_TOKEN_EXPIRES_IN,
        String.valueOf(expiresIn));
  }

  public static long getTokenAcquiredAt(AccountManager am, Account account) {
    String acquiredAt = am.getUserData(account, AccountAuthenticator.USERDATA_TOKEN_ACQUIRED_AT);
    return acquiredAt != null ? Long.parseLong(acquiredAt) : -1;
  }

  public static int getExpiresIn(AccountManager am, Account account) {
    String expiresIn = am.getUserData(account, AccountAuthenticator.USERDATA_TOKEN_EXPIRES_IN);
    return expiresIn != null ? Integer.parseInt(expiresIn) : -1;
  }
}
