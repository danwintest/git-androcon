package id.co.danwinciptaniaga.androcon.security;

import javax.inject.Inject;
import javax.inject.Singleton;

import android.content.Context;
import androidx.lifecycle.LiveData;
import id.co.danwinciptaniaga.androcon.auth.TokenResponse;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.repo.NetworkBoundResource;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;

@Singleton
public class UserRepo {
  private final Context context;
  private final AppExecutors appExecutors;

  @Inject
  public UserRepo(Context context, AppExecutors appExecutors) {
    this.context = context;
    this.appExecutors = appExecutors;
  }

  public LiveData<Resource<TokenResponse>> getToken(String username, String password) {
    NetworkBoundResource<TokenResponse, TokenResponse> result = new NetworkBoundResource<TokenResponse, TokenResponse>(
        appExecutors) {
      @Override
      protected void saveCallResult(TokenResponse item) {
        // @todo simpan AuthResponse ke DB lokal?
      }

      @Override
      protected Boolean shouldFetch(TokenResponse data) {
        return data == null;
      }

      @Override
      protected LiveData<TokenResponse> loadFromDb() {
        return null;
      }

      @Override
      protected LiveData<ApiResponse<TokenResponse>> createCall() {
//        AuthService authService = WebServiceGenerator.getInstance().createService(context,
//            AuthService.class);
//        return authService.loginLiveData(username, password, "password");
        return null;
      }
    };
    return result.asLiveData();
  }
}
