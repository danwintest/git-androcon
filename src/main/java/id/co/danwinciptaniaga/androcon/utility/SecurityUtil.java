package id.co.danwinciptaniaga.androcon.utility;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import androidx.annotation.RequiresApi;

public class SecurityUtil {

  public static final String ANDROID_KEY_STORE = "AndroidKeyStore";

  @RequiresApi(api = Build.VERSION_CODES.M)
  public static void generateSecretKey(String keyName)
      throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
    KeyGenParameterSpec.Builder kgpsBuilder = new KeyGenParameterSpec.Builder(
        keyName,
        KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
        .setUserAuthenticationRequired(true);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      // Invalidate the keys if the user has registered a new biometric
      // credential, such as a new fingerprint. Can call this method only
      // on Android 7.0 (API level 24) or higher. The variable
      // "invalidatedByBiometricEnrollment" is true by default.
      kgpsBuilder.setInvalidatedByBiometricEnrollment(true);
    }
    KeyGenerator keyGenerator = KeyGenerator.getInstance(
        KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEY_STORE);
    keyGenerator.init(kgpsBuilder.build());
    keyGenerator.generateKey();
  }

  public static SecretKey getSecretKey(String keyName)
      throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException,
      UnrecoverableKeyException {
    KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");

    // Before the keystore can be accessed, it must be loaded.
    keyStore.load(null);
    return ((SecretKey) keyStore.getKey(keyName, null));
  }

  public static Cipher getCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
    return Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
        + KeyProperties.BLOCK_MODE_CBC + "/"
        + KeyProperties.ENCRYPTION_PADDING_PKCS7);
  }

  public static Cipher getInitializedCipherForEncrypt(String keyName)
      throws NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException,
      CertificateException, KeyStoreException, IOException, InvalidKeyException {
    SecretKey secretKey = getSecretKey(keyName);
    Cipher cipher = getCipher();
    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    return cipher;
  }

  public static Cipher getInitializedCipherForDecrypt(String keyName, byte[] iv)
      throws NoSuchAlgorithmException, NoSuchPaddingException, UnrecoverableKeyException,
      CertificateException, KeyStoreException, IOException, InvalidKeyException,
      InvalidAlgorithmParameterException {
    SecretKey secretKey = getSecretKey(keyName);
    Cipher cipher = getCipher();
    cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
    return cipher;
  }
}
