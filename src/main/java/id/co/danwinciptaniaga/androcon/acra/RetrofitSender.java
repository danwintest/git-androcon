package id.co.danwinciptaniaga.androcon.acra;

import org.acra.collections.ImmutableSet;
import org.acra.data.CrashReportData;
import org.acra.data.StringFormat;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import androidx.annotation.NonNull;

public class RetrofitSender implements ReportSender {
  private static final String TAG = RetrofitSender.class.getSimpleName();
  private AcraService acraService;
  private StringFormat stringFormat;

  public RetrofitSender(AcraService acraService, StringFormat stringFormat) {
    this.acraService = acraService;
    this.stringFormat = stringFormat;
  }

  @Override
  public void send(@NonNull Context context, @NonNull CrashReportData errorContent)
      throws ReportSenderException {
    try {
      acraService.sendCrashData(
          stringFormat.toFormattedString(errorContent, ImmutableSet.empty(), "", "", false));
    } catch (Exception e) {
      HyperLog.w(TAG, "Problem sending Acra CrashReportData", e);
      throw new ReportSenderException("Problem sending Acra CrashReportData", e);
    }
  }
}
