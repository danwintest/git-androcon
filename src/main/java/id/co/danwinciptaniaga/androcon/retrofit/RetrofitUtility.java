package id.co.danwinciptaniaga.androcon.retrofit;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class RetrofitUtility {
  private static final String TAG = RetrofitUtility.class.getSimpleName();
  private static Pattern htmlTitle = Pattern.compile("<title>(.*?)</title>", Pattern.DOTALL);;

  @NonNull
  public static MultipartBody.Part prepareFilePart(@NonNull String partName, @NonNull File file,
      @NonNull String mediaType) {
    // create RequestBody instance from file
    RequestBody requestFile = RequestBody.create(file, MediaType.parse(mediaType));

    // MultipartBody.Part is used to send also the actual file name
    return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
  }

  public static ErrorResponse parseErrorBody(ResponseBody responseBody) {
    ErrorResponse errorResponse = null;
    if (responseBody != null) {
      String contentType =
          responseBody.contentType() != null ? responseBody.contentType().toString() : null;
      if (contentType != null &&
          contentType.indexOf(
              com.google.common.net.MediaType.create("application", "json").toString()) > -1) {
        try {
          JsonParser jp = new JsonParser();
          JsonObject jo = jp.parse(responseBody.charStream()).getAsJsonObject();
          JsonPrimitive error = jo.getAsJsonPrimitive("error");
          JsonPrimitive errorDescription = null;
          if (jo.has("error_description")) {
            // ini dari Oauth2Exception
            errorDescription = jo.getAsJsonPrimitive("error_description");
          } else if (jo.has("details")) {
            // ini dari ErrorInfo CP
            errorDescription = jo.getAsJsonPrimitive("details");
          }

          errorResponse = new ErrorResponse();
          if (error != null) {
            errorResponse.setError(error.getAsString());
          }
          if (errorDescription != null) {
            errorResponse.setErrorDescription(errorDescription.getAsString());
          }
        } catch (Exception e) {
          HyperLog.exception(TAG, "Problem parsing JSON error response", e);
          errorResponse = null;
        }
      } else {
        errorResponse = new ErrorResponse();
        try {
          errorResponse.setError("error");
          String content = responseBody.string();
          if (content != null) {
            Matcher m = htmlTitle.matcher(content);
            if (m.find()) {
              errorResponse.setErrorDescription(m.group(1));
            }
          }
        } catch (IOException e) {
          HyperLog.exception(TAG, "Problem parsing error", e);
        }
      }
    }
    return errorResponse;
  }

  public static ErrorResponse parseErrorBody(Throwable throwable) {
    ErrorResponse errorResponse = null;
    if (throwable != null) {
      if (HttpException.class.isAssignableFrom(throwable.getClass())) {
        errorResponse = RetrofitUtility.parseErrorBody(
            ((HttpException) throwable).response().errorBody());
      } else {
        errorResponse = new ErrorResponse();
        errorResponse.setError(throwable.getClass().getSimpleName());
        errorResponse.setErrorDescription(throwable.getMessage());
      }
    }
    return errorResponse;
  }
}