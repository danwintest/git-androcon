package id.co.danwinciptaniaga.androcon.registration;

import java.io.IOException;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.os.Build;
import id.co.danwinciptaniaga.acs.data.DeviceRegistrationResponse;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import retrofit2.Call;
import retrofit2.Response;

public class RegisterDeviceUseCaseSync {
  private static final String TAG = RegisterDeviceUseCaseSync.class.getSimpleName();
  private final AuthorizationHeaderProvider ahp;
  private final AndroconConfig androconConfig;
  private final DeviceRegistrationService deviceRegistrationService;
  private final RegistrationUtil registrationUtil;

  @Inject
  public RegisterDeviceUseCaseSync(AuthorizationHeaderProvider ahp, AndroconConfig androconConfig,
      DeviceRegistrationService deviceRegistrationService, RegistrationUtil registrationUtil) {
    this.ahp = ahp;
    this.androconConfig = androconConfig;
    this.deviceRegistrationService = deviceRegistrationService;
    this.registrationUtil = registrationUtil;
  }

  public boolean registerToServer() throws IOException {
    Call<DeviceRegistrationResponse> c = this.deviceRegistrationService
        .registerDevice(ahp.clientAuthorizationHeader(), Build.PRODUCT, Build.MODEL,
            Build.VERSION.RELEASE, androconConfig.isRootDetected(), androconConfig.getFcmToken());
    if (androconConfig.getAppDeviceId() == null) {
      throw new IllegalStateException(
          "ADID is not yet generated, cannot proceed with registration");
    }
    boolean result = false;
    try {
      Response<DeviceRegistrationResponse> r = c.execute();
      HyperLog.d(TAG, "Response Code: " + r.code());
      if (r.code() == 200 && r.body() != null) {
        if (this.androconConfig.getAppDeviceId().equals(r.body().getApplicationDeviceId())) {
          // sukses
          this.registrationUtil.setRegisteredToServer(true);
          result = true;
        } else {
          HyperLog.w(TAG, "Register ADID " + androconConfig.getAppDeviceId()
              + " to server returns invalid result: " + r.body().getApplicationDeviceId());
          this.registrationUtil.setRegisteredToServer(false);
        }
        this.registrationUtil.setByPassRootDetection(r.body().isByPassRootDetection());
      } else {
        this.registrationUtil.setRegisteredToServer(false);
      }
    } catch (IOException e) {
      HyperLog.e(TAG, "Problem registering ADID to server", e);
      this.registrationUtil.setRegisteredToServer(false);
      throw e;
    }

    return result;
  }
}
