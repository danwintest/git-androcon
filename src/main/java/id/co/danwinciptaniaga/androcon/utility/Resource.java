package id.co.danwinciptaniaga.androcon.utility;

/**
 * <p>Class ini dipergunakan untuk komunikasi antara LiveModel dengan {@link androidx.lifecycle.LifecycleOwner}-nya.
 * Status dapat memberikan indikasi ketika proses sedang berjalan, berakhir dengan sukses, ataupun gagal.</p>
 *
 * @param <T>
 */
public class Resource<T> extends BaseResource<T, ErrorResponse> {

  private Resource(Status status, String message, T data, ErrorResponse errorResponse,
      Throwable exception) {
    super(status, message, data, errorResponse, exception);
  }

  public static class Builder {
    public static <T> Resource success(String message, T data) {
      return new Resource<T>(Status.SUCCESS, message, data, null, null);
    }

    public static <T> Resource error(String message) {
      return new Resource<T>(Status.ERROR, message, null, null, null);
    }

    public static <T> Resource error(String message, ErrorResponse errorResponse) {
      return new Resource<T>(Status.ERROR, message, null, errorResponse, null);
    }

    public static <T> Resource error(String message, ErrorResponse errorResponse,
        Throwable throwable) {
      return new Resource<T>(Status.ERROR, message, null, errorResponse, throwable);
    }

    public static <T> Resource error(String message, Throwable throwable) {
      return new Resource<T>(Status.ERROR, message, null, null, throwable);
    }

    public static <T> Resource error(String message, T data) {
      return new Resource<T>(Status.ERROR, message, data, null, null);
    }

    public static <T> Resource loading(String message, T data) {
      return new Resource<T>(Status.LOADING, message, data, null, null);
    }
  }

  public static <T> String getMessageFromError(Resource<T> resource, String defaultMessage) {
    if (resource == null)
      throw new IllegalArgumentException("resource must not be null");
    if (!Status.ERROR.equals(resource.getStatus()))
      throw new IllegalArgumentException("resource must be an error");
    String result = null;
    if (resource.getMessage() != null) {
      result = resource.getMessage();
    } else if (resource.getErrorResponse() != null) {
      result = String.format("%s - %s", resource.getErrorResponse().getError(),
          resource.getErrorResponse().getErrorDescription());
    } else if (resource.getThrowable() != null) {
      result = resource.getThrowable().getMessage();
    } else {
      result = defaultMessage;
    }
    return result;
  }
}
