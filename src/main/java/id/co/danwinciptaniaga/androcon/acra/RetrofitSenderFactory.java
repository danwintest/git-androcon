package id.co.danwinciptaniaga.androcon.acra;

import org.acra.config.CoreConfiguration;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderFactory;

import com.google.auto.service.AutoService;

import android.content.Context;
import androidx.annotation.NonNull;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;

//@AutoService(ReportSenderFactory.class)
public class RetrofitSenderFactory implements ReportSenderFactory {
  @NonNull
  @Override
  public ReportSender create(@NonNull Context context, @NonNull CoreConfiguration config) {
//    AcraService acraService = WebServiceGenerator.getInstance().createService(context, AcraService.class);
//    return new RetrofitSender(acraService, config.reportFormat());
    return null;
  }

  @Override
  public boolean enabled(@NonNull CoreConfiguration config) {
    return true;
  }
}
