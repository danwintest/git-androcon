package id.co.danwinciptaniaga.androcon.security;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.OperationCanceledException;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import id.co.danwinciptaniaga.androcon.R;
import id.co.danwinciptaniaga.androcon.auth.AccountAuthenticator;
import id.co.danwinciptaniaga.androcon.auth.AccountManagerUtil;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public abstract class ProtectedActivity extends AppCompatActivity {
  protected final String TAG = getClass().getSimpleName();

  public static final long DEFAULT_SESSION_TIMEOUT = 3 * 60 * 1000; // 3 menit

  @Inject
  protected LoginUtil loginUtil;
  @Inject
  protected AppExecutors appExecutors;

  private AccountManager accountManager;
  protected ProtectedActivityViewModel model;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    accountManager = AccountManager.get(this);
    model = new ViewModelProvider(this).get(ProtectedActivityViewModel.class);
    model.getAuthenticationDone().observe(this, this::authenticationStatusUpdate);
    model.getPermissionsLd().observe(this, permissions -> {
      if (permissions != null) {
        switch (permissions.getStatus()) {
        case SUCCESS:
          model.setAuthenticationStatus(
              Resource.Builder.success(getString(R.string.authentication_progress_success), null));
          break;
        case ERROR:
          String message = null;
          if (permissions.getErrorResponse() != null) {
            message = permissions.getErrorResponse().getErrorDescription();
          }
          if (message == null && permissions.getMessage() != null) {
            message = permissions.getMessage();
          }
          model.setAuthenticationStatus(
              Resource.Builder.error(getString(R.string.authentication_progress_failed, message),
                  permissions.getErrorResponse()));
          break;
        }
      }
    });
  }

  @UiThread
  protected abstract void authenticationStatusUpdate(Resource<String> status);

  @Override
  protected void onResume() {
    super.onResume();
    model.setAuthenticationStatus(
        Resource.Builder.loading(getString(R.string.authentication_progress_loading), null));
    if (loginUtil.isLoggedIn()) {
      HyperLog.i(TAG, "User is logged in, checking session timeout");
      long lastPausedTime = loginUtil.getLastPausedTime();
      boolean shouldCheckToken = false;
      if (lastPausedTime > 0 || loginUtil.getSavedToken() == null) {
        // kalau lastPausedTime terdefinisi ATAU token kosong
        long pausedDuration = System.currentTimeMillis() - lastPausedTime;
        if (pausedDuration > getSessionTimoutDuration() || loginUtil.getSavedToken() == null) {
          // kalau timeout ATAU token kosong
          HyperLog.i(TAG,
              "Session has timeout or token has been cleared, confirming credentials to get token");
          String username = loginUtil.getUsername();
          Account account = loginUtil.getAccount(getProtectionAccountType());
          // pastikan account masih ada
          if (account != null) {
            loginUtil.clearToken();
            HyperLog.i(TAG,
                "Logged-in Account still exists, attempt to get its token by Confirming Credentials");
            // account masih ada, gunakan AccountManager untuk dapatkan token
            ListenableFuture<String> ccLf = Futures.submit(new Callable<String>() {
              @Override
              public String call() throws Exception {
                // tampilkan prompt untuk relogin
                Bundle arguments = new Bundle();
                arguments.putBoolean(AccountAuthenticator.ARG_PERFORM_APP_LOGIN, true);
                arguments.putString(AccountAuthenticator.ARG_AUTH_TOKEN_TYPE,
                    getProtectionAuthTokenType());
                Bundle bundle = accountManager.confirmCredentials(account, arguments,
                    ProtectedActivity.this, null, null).getResult();
                HyperLog.d(TAG, "confirmCredentials completes");
                boolean success = bundle.getBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
                String token = null;
                if (success) {
                  HyperLog.d(TAG, "confirmCredentials succeed");
                  // confirm berhasil, jadi kita bisa dapatkan token
                  token = accountManager.blockingGetAuthToken(account,
                      getProtectionAuthTokenType(), false);
                  HyperLog.d(TAG, "Obtained token after successful confirmCredentials");
                  if (TextUtils.isEmpty(token)) {
                    HyperLog.w(TAG, "Failed getting token from ConfirmCredentials sequence");
                    // toast ini menyebabkan error (Looper belum dipanggil)
                    //                    Toast.makeText(ProtectedActivity.this,
                    //                        getString(R.string.login_required_for_user, username),
                    //                        Toast.LENGTH_LONG).show();
                    finish();
                  } else {
                    HyperLog.i(TAG,
                        String.format("Token for %s obtained from Account Manager", username));
                    loginUtil.saveToken(token, getProtectionAuthTokenType());
                    loginUtil.clearLastPausedTime();
                    // trigger baca permissions
                    model.triggerGetPermissions();
                  }
                } else {
                  HyperLog.d(TAG, "confirmCredentials failed");
                  // toast ini menyebabkan error (Looper belum dipanggil)
                  //                  Toast.makeText(ProtectedActivity.this,
                  //                      getString(R.string.login_required_for_user, username),
                  //                      Toast.LENGTH_LONG).show();
                  finish();
                }
                return token;
              }
            }, appExecutors.backgroundIO());

          } else {
            HyperLog.i(TAG,
                "Logged-in Account no longer exists, logout");
            // account sudah tidak ada, harus logout
            logout();
            finish();
          }
        } else {
          HyperLog.i(TAG, "Session is still valid, not doing anything about token");
          shouldCheckToken = true;
        }
      } else {
        HyperLog.i(TAG, "Activity pause was not tracked, not doing anything about token");
        shouldCheckToken = true;
      }
      if (shouldCheckToken) {
        HyperLog.d(TAG, "Session check ok, checking if token should be refreshed");
        // token harus dicek lagi, karena bisa kosong atau invalid
        Account account = loginUtil.getAccount(getProtectionAccountType());
        boolean shouldGetToken = false;
        if (account != null) {
          String token = accountManager.peekAuthToken(account, getProtectionAuthTokenType());
          if (token != null) {
            // ada token cached, check expiry
            long acquiredAt = AccountManagerUtil.getTokenAcquiredAt(accountManager,
                account); // dalam ms
            int expiresIn = AccountManagerUtil.getExpiresIn(accountManager, account)
                * 1000; // dalam detik, jadi kali 1000
            long expiresAt = acquiredAt + expiresIn;
            HyperLog.d(TAG,
                String.format("Token expires in %s, acquired at %s, expires at %s", expiresIn,
                    acquiredAt, expiresAt));
            boolean shouldRefresh = loginUtil.shouldRefreshToken(expiresAt,
                LoginUtil.MIN_TIME_TO_REFRESH);
            if (shouldRefresh) {
              HyperLog.d(TAG, "Should refresh token, invalidating");
              accountManager.invalidateAuthToken(getProtectionAccountType(), token);
              shouldGetToken = true;
            } else {
              HyperLog.d(TAG, "No need to refresh token");
            }
          } else {
            HyperLog.d(TAG, "No cached token, must get Token");
            shouldGetToken = true;
          }
          if (shouldGetToken) {
            HyperLog.d(TAG, "Attempting to refresh token by calling getAuthToken");
            Bundle args = new Bundle();
            args.putBoolean(AccountAuthenticator.ARG_PERFORM_APP_LOGIN, true);
            ListenableFuture<Bundle> authTokenLf = AccountManagerUtil.getAuthTokenLf(accountManager,
                account, getProtectionAuthTokenType(), args, appExecutors.backgroundIO());

            Futures.addCallback(authTokenLf, new FutureCallback<Bundle>() {
              @Override
              public void onSuccess(@NullableDecl Bundle result) {
                HyperLog.d(TAG,
                    "Attempt to refresh auth token completes successfully: " + result.getString(
                        AccountManager.KEY_AUTHTOKEN));
                // trigger get permissions
                model.triggerGetPermissions();
              }

              @Override
              public void onFailure(Throwable t) {
                HyperLog.e(TAG, "Attempt to refresh auth token failed", t);
                model.setAuthenticationStatus(
                    Resource.Builder.error(getString(R.string.authentication_progress_failed, t),
                        t));
              }
            }, appExecutors.mainThread());
          } else {
            // langsung trigger get permissions dengan token yg ada
            model.triggerGetPermissions();
          }
        } else {
          Log.w(TAG, "No account??");
          model.setAuthenticationStatus(Resource.Builder.error(
              getString(R.string.authentication_progress_failed, "no account")));
        }
      }
    } else {
      HyperLog.i(TAG,
          "User is not logged-in, skip checking session timeout, call addAccount immediately");
      ListenableFuture<Bundle> addAccountLf = Futures.submit(new Callable<Bundle>() {
        @Override
        public Bundle call() throws Exception {
          return accountManager.addAccount(getProtectionAccountType(), getProtectionAuthTokenType(),
              null, null, ProtectedActivity.this, null, null)
              .getResult();
        }
      }, appExecutors.backgroundIO());
      Futures.addCallback(addAccountLf, new FutureCallback<Bundle>() {
        @Override
        public void onSuccess(@NullableDecl Bundle result) {
          // harusnya result tidak pernah mengembalikan Intent, karena addAccount yang akan membuka
          if (result.get(AccountManager.KEY_INTENT) != null)
            throw new IllegalStateException("During login, addAccount should not return Intent");
          // Activity yang diperlukan untuk Authentication
          String username = result.getString(AccountManager.KEY_ACCOUNT_NAME);
          HyperLog.i(TAG, "Successful login for " + username);
          // trigger baca permission
          model.triggerGetPermissions();
        }

        @Override
        public void onFailure(Throwable t) {
          if (OperationCanceledException.class.isAssignableFrom(t.getClass())) {
            HyperLog.w(TAG, "Attempt to prompt User to login is cancelled");
            finish();
          } else {
            HyperLog.e(TAG, "Error while attempting to prompt User to login", t);
            Toast.makeText(ProtectedActivity.this,
                ProtectedActivity.this.getString(R.string.generic_error, t.getMessage()),
                Toast.LENGTH_LONG).show();
            finish();
          }
        }
      }, appExecutors.backgroundIO());
    }
  }

  /**
   * Subclass bisa mengoverride method ini untuk mengubah durasi session timeout ketika aplikasi ditinggalkan.
   * Timeout dihitung dari waktu (millisecond) antara activity onPaused dan onResume kembali.
   *
   * @return
   */
  protected long getSessionTimoutDuration() {
    return DEFAULT_SESSION_TIMEOUT;
  }

  @Override
  protected void onPause() {
    super.onPause();
    long now = System.currentTimeMillis();
    if (loginUtil.isLoggedIn()) {
      loginUtil.setLastPausedTime(now);
      HyperLog.i(TAG, "Device paused at " + now + ", tracking ");
    } else {
      HyperLog.i(TAG, "Device paused at " + now + ", but not tracking");
    }
  }

  @NonNull
  protected abstract String getProtectionAccountType();

  @NonNull
  protected abstract String getProtectionAuthTokenType();

  protected void logout() {
    // logout internal
    model.logout();
  }
}
