package id.co.danwinciptaniaga.androcon.acra;

import retrofit2.Call;
import retrofit2.http.POST;

public interface AcraService {
  @POST("/crashlogs")
  Call<String> sendCrashData(Object o);
}
