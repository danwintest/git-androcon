package id.co.danwinciptaniaga.androcon.auth;

import com.google.gson.annotations.SerializedName;

public class UserInfo {

  @SerializedName("firstName")
  private String firstName;

  @SerializedName("lastName")
  private String lastName;

  @SerializedName("name")
  private String name;

  @SerializedName("timeZone")
  private String timeZone;

  @SerializedName("middleName")
  private String middleName;

  @SerializedName("language")
  private String language;

  @SerializedName("id")
  private String id;

  @SerializedName("position")
  private String position;

  @SerializedName("_instanceName")
  private String instanceName;

  @SerializedName("login")
  private String login;

  @SerializedName("locale")
  private String locale;

  @SerializedName("email")
  private String email;

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getName() {
    return name;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public String getMiddleName() {
    return middleName;
  }

  public String getLanguage() {
    return language;
  }

  public String getId() {
    return id;
  }

  public String getPosition() {
    return position;
  }

  public String getInstanceName() {
    return instanceName;
  }

  public String getLogin() {
    return login;
  }

  public String getLocale() {
    return locale;
  }

  public String getEmail() {
    return email;
  }
}