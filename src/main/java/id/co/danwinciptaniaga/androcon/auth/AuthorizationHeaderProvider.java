package id.co.danwinciptaniaga.androcon.auth;

import javax.inject.Inject;

import android.util.Base64;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.utility.AndroconUtility;

public class AuthorizationHeaderProvider {
  private AndroconConfig androconConfig;
  private LoginUtil loginUtil;

  @Inject
  public AuthorizationHeaderProvider(AndroconConfig androconConfig, LoginUtil loginUtil) {
    this.androconConfig = androconConfig;
    this.loginUtil = loginUtil;
  }

  private volatile String clientAuthorization = null;
  private volatile String tokenAuthorization = null;

  public String clientAuthorizationHeader() {
    if (clientAuthorization == null) {
      clientAuthorization = AndroconUtility.clientAuthorizationHeader(androconConfig);
    }
    return clientAuthorization;
  }

  public String tokenAuthorizationHeader() {
    return tokenAuthorizationHeader(loginUtil.getSavedToken());
  }

  public String tokenAuthorizationHeader(String token) {
    if (tokenAuthorization == null) {
      tokenAuthorization = AndroconUtility.tokenAuthorizationHeader(token);
    }
    return tokenAuthorization;
  }
}
