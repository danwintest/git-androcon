package id.co.danwinciptaniaga.androcon;

public interface HasAccountType {
  String getAccountType();
  String getAuthTokenType();
}
