package id.co.danwinciptaniaga.androcon.auth;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetUserInfoUseCase
    extends BaseObservable<GetUserInfoUseCase.Listener, UserInfo> {
  private static final String TAG = GetUserInfoUseCase.class.getSimpleName();

  public interface Listener {
    void onGetUserInfoStarted();

    void onGetUserInfoSuccess(Resource<UserInfo> response);

    void onGetUserInfoFailure(Resource<UserInfo> responseError);
  }

  private final UserService userService;
  private final AppExecutors appExecutors;

  @Inject
  public GetUserInfoUseCase(Application application, UserService userService,
      AppExecutors appExecutors) {
    super(application);
    this.userService = userService;
    this.appExecutors = appExecutors;
  }

  public void getUserInfo() {
    HyperLog.i(TAG, "getUserInfo called");
    notifyStart(null, null);
    Call<UserInfo> arCall = userService.getUserCall();
    arCall.enqueue(new Callback<UserInfo>() {
      @Override
      public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
        HyperLog.i(TAG, String.format("getUserInfo onResponse: %s", response));
        appExecutors.networkIO().execute(
            () -> {
              ApiResponse<UserInfo> apiResponse = ApiResponse.create(response);
              HyperLog.i(TAG, String.format("getUserInfo apiResponse: %s", apiResponse));
              if (apiResponse instanceof ApiResponse.ApiSuccessResponse) {
                UserInfo userInfo = ((ApiResponse.ApiSuccessResponse<UserInfo>) apiResponse).getData();
                notifySuccess(null, userInfo);
              } else if (apiResponse instanceof ApiResponse.ApiErrorResponse) {
                ApiResponse.ApiErrorResponse<UserInfo> aer = (ApiResponse.ApiErrorResponse) apiResponse;
                notifyFailure(null, aer.getErrorResponse(), null);
              }
            });
      }

      @Override
      public void onFailure(Call<UserInfo> call, Throwable t) {
        HyperLog.i(TAG, String.format("getUserInfo onFailure: %s", t));
        appExecutors.networkIO().execute(
            () -> {
              notifyFailure(null, null, t);
            });
      }
    });
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<UserInfo> result) {
    listener.onGetUserInfoStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<UserInfo> result) {
    listener.onGetUserInfoSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<UserInfo> result) {
    listener.onGetUserInfoFailure(result);
  }
}