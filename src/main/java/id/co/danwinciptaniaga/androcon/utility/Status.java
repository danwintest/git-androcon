package id.co.danwinciptaniaga.androcon.utility;

public enum Status {
  SUCCESS,
  ERROR,
  LOADING
}
