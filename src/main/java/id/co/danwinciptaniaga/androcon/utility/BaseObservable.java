package id.co.danwinciptaniaga.androcon.utility;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import android.app.Application;

public abstract class BaseObservable<LISTENER_CLASS, RETURN_RESULT> {
  private final Object MONITOR = new Object();
  protected final Application application;

  public BaseObservable(Application application) {
    this.application = application;
  }

  private final Set<LISTENER_CLASS> mListeners = new HashSet<>();

  public void registerListener(LISTENER_CLASS listener) {
    synchronized (MONITOR) {
      boolean hadNoListeners = mListeners.size() == 0;
      mListeners.add(listener);
      if (hadNoListeners && mListeners.size() == 1) {
        onFirstListenerRegistered();
      }
    }
  }

  public void unregisterListener(LISTENER_CLASS listener) {
    synchronized (MONITOR) {
      boolean hadOneListener = mListeners.size() == 1;
      mListeners.remove(listener);
      if (hadOneListener && mListeners.size() == 0) {
        onLastListenerUnregistered();
      }
    }
  }

  protected Set<LISTENER_CLASS> getListeners() {
    synchronized (MONITOR) {
      return Collections.unmodifiableSet(new HashSet<>(mListeners));
    }
  }

  protected void onFirstListenerRegistered() {

  }

  protected void onLastListenerUnregistered() {

  }

  /**
   * Method ini dipanggil dalam method utama UseCase untuk menandakan proses dimulai.
   *
   * @param message
   * @param returnResult
   */
  protected void notifyStart(String message, RETURN_RESULT returnResult) {
    for (LISTENER_CLASS listener : getListeners()) {
      doNotifyStart(listener, Resource.Builder.loading(message, returnResult));
    }
  }

  /**
   * Subclass harus mengimplementasikan method ini untuk memanggil nama method penanda mulai pada class Listener-nya.
   *
   * @param listener
   * @param result
   */
  protected abstract void doNotifyStart(LISTENER_CLASS listener, Resource<RETURN_RESULT> result);

  /**
   * Method ini dipanggil dalam method utama UseCase untuk menandakan proses berhasil.
   *
   * @param message
   * @param returnResult
   */
  protected void notifySuccess(String message, RETURN_RESULT returnResult) {
    for (LISTENER_CLASS listener : getListeners()) {
      doNotifySuccess(listener, Resource.Builder.success(message, returnResult));
    }
  }

  /**
   * Subclass harus mengimplementasikan method ini untuk memanggil nama method penanda sukses pada class Listener-nya.
   *
   * @param listener
   * @param result
   */
  protected abstract void doNotifySuccess(LISTENER_CLASS listener, Resource<RETURN_RESULT> result);

  /**
   * Method ini dipanggil dalam method utama UseCase untuk menandakan proses gagal.
   *
   * @param message
   * @param throwable
   */
  protected void notifyFailure(String message, ErrorResponse errorData, Throwable throwable) {
    for (LISTENER_CLASS listener : getListeners()) {
      doNotifyFailure(listener, Resource.Builder.error(message, errorData, throwable));
    }
  }

  /**
   * Subclass harus mengimplementasikan method ini untuk memanggil nama method penanda gagal pada class Listener-nya.
   *
   * @param listener
   * @param result
   */
  protected abstract void doNotifyFailure(LISTENER_CLASS listener, Resource<RETURN_RESULT> result);
}
