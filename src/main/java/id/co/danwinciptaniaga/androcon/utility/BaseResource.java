package id.co.danwinciptaniaga.androcon.utility;

public class BaseResource<T, ER> {
  protected final Status status;
  protected T data;
  protected String message;
  protected ER errorResponse;
  protected Throwable throwable;

  protected BaseResource(Status status, String message, T data) {
    this.status = status;
    this.message = message;
    this.data = data;
  }

  protected BaseResource(Status status, String message, ER errorResponse, Throwable throwable) {
    this.status = status;
    this.message = message;
    this.errorResponse = errorResponse;
    this.throwable = throwable;
  }

  protected BaseResource(Status status, String message, T data, ER errorResponse,
      Throwable throwable) {
    this.status = status;
    this.message = message;
    this.data = data;
    this.errorResponse = errorResponse;
    this.throwable = throwable;
  }

  public Status getStatus() {
    return status;
  }

  public T getData() {
    return data;
  }

  public String getMessage() {
    return message;
  }

  public ER getErrorResponse() {
    return errorResponse;
  }

  public Throwable getThrowable() {
    return throwable;
  }

  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append("Resource[")
        .append("status=").append(status.name())
        .append(", message=").append(message)
        .append(", data=").append(data != null ? data.toString() : null)
        .append("]");

    return sb.toString();
  }

  public static class Builder {
    public static <T, ER> BaseResource success(String message, T data) {
      return new BaseResource<T, ER>(Status.SUCCESS, message, data);
    }

    public static <T, ER> BaseResource error(String message, ER errorResponse) {
      return new BaseResource<T, ER>(Status.ERROR, message, errorResponse, null);
    }

    public static <T, ER> BaseResource error(String message, ER errorResponse,
        Throwable throwable) {
      return new BaseResource<T, ER>(Status.ERROR, message, errorResponse, throwable);
    }

    public static <T, ER> BaseResource error(String message, Throwable throwable) {
      return new BaseResource<T, ER>(Status.ERROR, message, null, throwable);
    }

    public static <T, ER> BaseResource loading(String message, T data) {
      return new BaseResource<T, ER>(Status.LOADING, message, data);
    }
  }
}
