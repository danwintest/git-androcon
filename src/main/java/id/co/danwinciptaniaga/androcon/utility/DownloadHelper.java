package id.co.danwinciptaniaga.androcon.utility;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import id.co.danwinciptaniaga.androcon.R;

public class DownloadHelper {

  public static Cursor queryDownload(DownloadManager dm, long downloadId) {
    DownloadManager.Query query = new DownloadManager.Query().setFilterById(downloadId);
    Cursor cursor = dm.query(query);
    return cursor;
  }

  public static int getDownloadStatus(Cursor cursor) {
    return cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
  }

  public static long getBytesDownloaded(Cursor cursor) {
    return cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
  }

  public static long getTotalSizeBytes(Cursor cursor) {
    return cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
  }

  public static int getReason(Cursor cursor) {
    int result = -1;
    int status = getDownloadStatus(cursor);
    switch (status) {
    case DownloadManager.STATUS_FAILED:
    case DownloadManager.STATUS_PAUSED:
      result = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
      break;
    default:
      throw new IllegalStateException(
          "Reason is undefined for status other than Failed and Paused");
    }
    return result;
  }

  public static String getReasonString(Context context, Cursor cursor) {
    int reason = getReason(cursor);
    String result;
    switch (reason) {
    case DownloadManager.ERROR_UNKNOWN:
      result = context.getString(R.string.download_error_unknown);
      break;
    case DownloadManager.ERROR_FILE_ERROR:
      result = context.getString(R.string.download_error_file_error);
      break;
    case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
      result = context.getString(R.string.download_error_unhandled_http_code);
      break;
    case DownloadManager.ERROR_HTTP_DATA_ERROR:
      result = context.getString(R.string.download_error_http_data_error);
      break;
    case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
      result = context.getString(R.string.download_error_too_many_redirects);
      break;
    case DownloadManager.ERROR_INSUFFICIENT_SPACE:
      result = context.getString(R.string.download_error_insufficient_space);
      break;
    case DownloadManager.ERROR_DEVICE_NOT_FOUND:
      result = context.getString(R.string.download_error_device_not_found);
      break;
    case DownloadManager.ERROR_CANNOT_RESUME:
      result = context.getString(R.string.download_error_cannot_resume);
      break;
    case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
      result = context.getString(R.string.download_error_file_already_exists);
      break;
    case DownloadManager.PAUSED_WAITING_TO_RETRY:
      result = context.getString(R.string.download_error_paused_waiting_to_retry);
      break;
    case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
      result = context.getString(R.string.download_error_paused_waiting_for_network);
      break;
    case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
      result = context.getString(R.string.download_error_paused_queued_for_wifi);
      break;
    case DownloadManager.PAUSED_UNKNOWN:
      result = context.getString(R.string.download_error_paused_unknown);
      break;
    default:
      result = context.getString(R.string.download_error_unknown);
    }
    return result;
  }
}
