package id.co.danwinciptaniaga.androcon.utility;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import androidx.preference.PreferenceManager;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;

public class AndroconUtility {
  private static final String TAG = AndroconUtility.class.getSimpleName();

  public static final String HTTP_HEADER_ADID = "ADID";
  public static final String HTTP_HEADER_DID = "DID";

  private static String clientAuthorization = null;
  private static String tokenAuthorization = null;

  public static String clientAuthorizationHeader(AndroconConfig androconConfig) {
    if (clientAuthorization == null) {
      String s = String.format("%s:%s",
          androconConfig.getAndroconClientId(), androconConfig.getAndroconClientSecret());
      clientAuthorization = "Basic " + Base64.encodeToString(s.getBytes(), Base64.NO_WRAP);
    }
    return clientAuthorization;
  }

  public static String tokenAuthorizationHeader(String token) {
    if (token == null)
      throw new IllegalArgumentException("Token cannot be null");
    if (tokenAuthorization == null) {
      tokenAuthorization = "Bearer " + token;
    }
    return tokenAuthorization;
  }

}
