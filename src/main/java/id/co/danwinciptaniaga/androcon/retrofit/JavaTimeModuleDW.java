package id.co.danwinciptaniaga.androcon.retrofit;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.PackageVersion;
import com.fasterxml.jackson.datatype.jsr310.deser.DurationDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.JSR310StringParsableDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.MonthDayDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.OffsetTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.YearDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.YearMonthDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.DurationKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.InstantKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.LocalDateKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.LocalDateTimeKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.LocalTimeKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.MonthDayKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.OffsetDateTimeKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.OffsetTimeKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.PeriodKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.YearKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.YearMothKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.ZoneIdKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.ZoneOffsetKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.key.ZonedDateTimeKeyDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.DurationSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.InstantSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.MonthDaySerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.OffsetDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.OffsetTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.YearMonthSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.YearSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.key.ZonedDateTimeKeySerializer;

import android.annotation.SuppressLint;

public class JavaTimeModuleDW extends SimpleModule {

  private static final long serialVersionUID = 1L;
  @SuppressLint("NewApi")
  public JavaTimeModuleDW()
    {
      super(PackageVersion.VERSION);
      // First deserializers

      // // Instant variants:
        addDeserializer(Instant.class, InstantDeserializerDW.INSTANT);
        addDeserializer(OffsetDateTime.class, InstantDeserializerDW.OFFSET_DATE_TIME);
        addDeserializer(ZonedDateTime.class, InstantDeserializerDW.ZONED_DATE_TIME);
        // // Other deserializers
        addDeserializer(Duration.class, DurationDeserializer.INSTANCE);
        addDeserializer(LocalDateTime.class, LocalDateTimeDeserializer.INSTANCE);
        addDeserializer(LocalDate.class, LocalDateDeserializer.INSTANCE);
        addDeserializer(LocalTime.class, LocalTimeDeserializer.INSTANCE);
        addDeserializer(MonthDay.class, MonthDayDeserializer.INSTANCE);
        addDeserializer(OffsetTime.class, OffsetTimeDeserializer.INSTANCE);
        addDeserializer(Period.class, JSR310StringParsableDeserializer.PERIOD);
        addDeserializer(Year.class, YearDeserializer.INSTANCE);
        addDeserializer(YearMonth.class, YearMonthDeserializer.INSTANCE);
        addDeserializer(ZoneId.class, JSR310StringParsableDeserializer.ZONE_ID);
        addDeserializer(ZoneOffset.class, JSR310StringParsableDeserializer.ZONE_OFFSET);

        // then serializers:
        addSerializer(Duration.class, DurationSerializer.INSTANCE);
        addSerializer(Instant.class, InstantSerializer.INSTANCE);
        addSerializer(LocalDateTime.class, LocalDateTimeSerializer.INSTANCE);
        addSerializer(LocalDate.class, LocalDateSerializer.INSTANCE);
        addSerializer(LocalTime.class, LocalTimeSerializer.INSTANCE);
        addSerializer(MonthDay.class, MonthDaySerializer.INSTANCE);
        addSerializer(OffsetDateTime.class, OffsetDateTimeSerializer.INSTANCE);
        addSerializer(OffsetTime.class, OffsetTimeSerializer.INSTANCE);
        addSerializer(Period.class, new ToStringSerializer(Period.class));
        addSerializer(Year.class, YearSerializer.INSTANCE);
        addSerializer(YearMonth.class, YearMonthSerializer.INSTANCE);

        /* 27-Jun-2015, tatu: This is the real difference from the old
         *  {@link JSR310Module}: default is to produce ISO-8601 compatible
         *  serialization with timezone offset only, not timezone id.
         *  But this is configurable.
         */
        addSerializer(ZonedDateTime.class, ZonedDateTimeSerializer.INSTANCE);

        // note: actual concrete type is `ZoneRegion`, but that's not visible:
        addSerializer(ZoneId.class, new ToStringSerializer(ZoneId.class));

        addSerializer(ZoneOffset.class, new ToStringSerializer(ZoneOffset.class));

        // key serializers
        addKeySerializer(ZonedDateTime.class, ZonedDateTimeKeySerializer.INSTANCE);

        // key deserializers
        addKeyDeserializer(Duration.class, DurationKeyDeserializer.INSTANCE);
        addKeyDeserializer(Instant.class, InstantKeyDeserializer.INSTANCE);
        addKeyDeserializer(LocalDateTime.class, LocalDateTimeKeyDeserializer.INSTANCE);
        addKeyDeserializer(LocalDate.class, LocalDateKeyDeserializer.INSTANCE);
        addKeyDeserializer(LocalTime.class, LocalTimeKeyDeserializer.INSTANCE);
        addKeyDeserializer(MonthDay.class, MonthDayKeyDeserializer.INSTANCE);
        addKeyDeserializer(OffsetDateTime.class, OffsetDateTimeKeyDeserializer.INSTANCE);
        addKeyDeserializer(OffsetTime.class, OffsetTimeKeyDeserializer.INSTANCE);
        addKeyDeserializer(Period.class, PeriodKeyDeserializer.INSTANCE);
        addKeyDeserializer(Year.class, YearKeyDeserializer.INSTANCE);
        addKeyDeserializer(YearMonth.class, YearMothKeyDeserializer.INSTANCE);
        addKeyDeserializer(ZonedDateTime.class, ZonedDateTimeKeyDeserializer.INSTANCE);
        addKeyDeserializer(ZoneId.class, ZoneIdKeyDeserializer.INSTANCE);
        addKeyDeserializer(ZoneOffset.class, ZoneOffsetKeyDeserializer.INSTANCE);
//      }else{
//        // First deserializers
//
//        // // Instant variants:
//        addDeserializer(
//            org.threeten.bp.Instant.class, com.fasterxml.jackson.datatype.threetenbp.deser.InstantDeserializer.INSTANT);
//        addDeserializer(
//            org.threeten.bp.OffsetDateTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.InstantDeserializer.OFFSET_DATE_TIME);
//        addDeserializer(
//            org.threeten.bp.ZonedDateTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.InstantDeserializer.ZONED_DATE_TIME);
//
//        // // Other deserializers
//        addDeserializer(
//            org.threeten.bp.Duration.class, com.fasterxml.jackson.datatype.threetenbp.deser.DurationDeserializer.INSTANCE);
//        addDeserializer(
//            org.threeten.bp.LocalDateTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.LocalDateTimeDeserializer.INSTANCE);
//        addDeserializer(
//            org.threeten.bp.LocalDate.class, com.fasterxml.jackson.datatype.threetenbp.deser.LocalDateDeserializer.INSTANCE);
//        addDeserializer(
//            org.threeten.bp.LocalTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.LocalTimeDeserializer.INSTANCE);
//        addDeserializer(
//            org.threeten.bp.MonthDay.class, com.fasterxml.jackson.datatype.threetenbp.deser.MonthDayDeserializer.INSTANCE);
//        addDeserializer(
//            org.threeten.bp.OffsetTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.OffsetTimeDeserializer.INSTANCE);
//        addDeserializer(org.threeten.bp.Period.class, ThreeTenStringParsableDeserializer.PERIOD);
//        addDeserializer(
//            org.threeten.bp.Year.class, com.fasterxml.jackson.datatype.threetenbp.deser.YearDeserializer.INSTANCE);
//        addDeserializer(
//            org.threeten.bp.YearMonth.class, com.fasterxml.jackson.datatype.threetenbp.deser.YearMonthDeserializer.INSTANCE);
//        addDeserializer(org.threeten.bp.ZoneId.class, ThreeTenStringParsableDeserializer.ZONE_ID);
//        addDeserializer(org.threeten.bp.ZoneOffset.class, ThreeTenStringParsableDeserializer.ZONE_OFFSET);
//
//        // then serializers:
//        addSerializer(
//            org.threeten.bp.Duration.class, com.fasterxml.jackson.datatype.threetenbp.ser.DurationSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.Instant.class, com.fasterxml.jackson.datatype.threetenbp.ser.InstantSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.LocalDateTime.class, com.fasterxml.jackson.datatype.threetenbp.ser.LocalDateTimeSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.LocalDate.class, com.fasterxml.jackson.datatype.threetenbp.ser.LocalDateSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.LocalTime.class, com.fasterxml.jackson.datatype.threetenbp.ser.LocalTimeSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.MonthDay.class, com.fasterxml.jackson.datatype.threetenbp.ser.MonthDaySerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.OffsetDateTime.class, com.fasterxml.jackson.datatype.threetenbp.ser.OffsetDateTimeSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.OffsetTime.class, com.fasterxml.jackson.datatype.threetenbp.ser.OffsetTimeSerializer.INSTANCE);
//        addSerializer(org.threeten.bp.Period.class, new ToStringSerializer(org.threeten.bp.Period.class));
//        addSerializer(
//            org.threeten.bp.Year.class, com.fasterxml.jackson.datatype.threetenbp.ser.YearSerializer.INSTANCE);
//        addSerializer(
//            org.threeten.bp.YearMonth.class, com.fasterxml.jackson.datatype.threetenbp.ser.YearMonthSerializer.INSTANCE);
//
//        /* 27-Jun-2015, tatu: This is the real difference from the old
//         *  {@link ThreeTenModule}: default is to produce ISO-8601 compatible
//         *  serialization with timezone offset only, not timezone id.
//         *  But this is configurable.
//         */
//        addSerializer(
//            org.threeten.bp.ZonedDateTime.class, com.fasterxml.jackson.datatype.threetenbp.ser.ZonedDateTimeSerializer.INSTANCE);
//
//        // since 2.11: need to override Type Id handling
//        // (actual concrete type is `ZoneRegion`, but that's not visible)
//        addSerializer(org.threeten.bp.ZoneId.class, new ZoneIdSerializer());
//        addSerializer(
//            org.threeten.bp.ZoneOffset.class, new ToStringSerializer(org.threeten.bp.ZoneOffset.class));
//
//        // key serializers
//        addKeySerializer(
//            org.threeten.bp.ZonedDateTime.class, com.fasterxml.jackson.datatype.threetenbp.ser.key.ZonedDateTimeKeySerializer.INSTANCE);
//
//        // key deserializers
//        addKeyDeserializer(
//            org.threeten.bp.Duration.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.DurationKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.Instant.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.InstantKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.LocalDateTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.LocalDateTimeKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.LocalDate.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.LocalDateKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.LocalTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.LocalTimeKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.MonthDay.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.MonthDayKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.OffsetDateTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.OffsetDateTimeKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.OffsetTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.OffsetTimeKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.Period.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.PeriodKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.Year.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.YearKeyDeserializer.INSTANCE);
//        addKeyDeserializer(org.threeten.bp.YearMonth.class, YearMonthKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.ZonedDateTime.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.ZonedDateTimeKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.ZoneId.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.ZoneIdKeyDeserializer.INSTANCE);
//        addKeyDeserializer(
//            org.threeten.bp.ZoneOffset.class, com.fasterxml.jackson.datatype.threetenbp.deser.key.ZoneOffsetKeyDeserializer.INSTANCE);
//      }
    }
}
