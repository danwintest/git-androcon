package id.co.danwinciptaniaga.androcon.utility;

import static android.content.ContentValues.TAG;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.exifinterface.media.ExifInterface;

public class ImageUtility {
  public static final String EXTENSION_JPG = ".jpg";

  public static int getImageOrientation(File file) throws IOException {
    ExifInterface exif = new ExifInterface(file);
    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_NORMAL);
    int rotate = 0;
    switch (orientation) {
    case ExifInterface.ORIENTATION_ROTATE_270:
      rotate = 270;
      break;
    case ExifInterface.ORIENTATION_ROTATE_180:
      rotate = 180;
      break;
    case ExifInterface.ORIENTATION_ROTATE_90:
      rotate = 90;
      break;
    }
    return rotate;
  }

  public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
    int width = image.getWidth();
    int height = image.getHeight();

    float bitmapRatio = (float) width / (float) height;
    if (bitmapRatio > 1) {
      width = maxSize;
      height = (int) (width / bitmapRatio);
    } else {
      height = maxSize;
      width = (int) (height * bitmapRatio);
    }
    return Bitmap.createScaledBitmap(image, width, height, true);
  }

  public static File saveBitmapToJpg(Context context, Bitmap bitmapImage, File parentDir,
      String directory, String filename) throws IOException {
    return saveBitmapToJpg(context, bitmapImage, 70, parentDir, directory, filename);
  }

  public static File saveBitmapToJpg(Context context, Bitmap bitmapImage, int quality,
      File parentDir,
      String directory, String filename) throws IOException {
    File compressedFile = createTempFile(parentDir, directory, filename, EXTENSION_JPG);

    HyperLog.i(TAG, "saveImage: " + compressedFile.getAbsolutePath());
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(compressedFile);
      // Use the compress method on the BitMap object to write image to the OutputStream
      bitmapImage.compress(Bitmap.CompressFormat.JPEG, quality, fos);
    } finally {
      try {
        fos.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return compressedFile;
  }

  public static File createTempFile(File parentDir, String directory,
      String filename, String fileExtension) throws IOException {
    File storageDir = new File(parentDir, directory);
    if (!storageDir.exists()) {
      HyperLog.i(TAG, "Creating " + storageDir.getAbsolutePath() + ": " + storageDir.mkdir());
    } else {
      HyperLog.i(TAG, storageDir.getAbsolutePath() + " exists");
    }
    File image = File.createTempFile(
        filename, // prefix
        fileExtension, // suffix
        storageDir // directory
    );

    return image;
  }
}
