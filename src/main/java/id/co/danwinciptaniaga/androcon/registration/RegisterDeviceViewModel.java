package id.co.danwinciptaniaga.androcon.registration;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public class RegisterDeviceViewModel extends ViewModel implements RegisterDeviceUseCase.Listener {
  private MutableLiveData<Resource> deviceRegistrationStatus = new MutableLiveData<>(null);

  private RegisterDeviceUseCase registerDeviceUseCase;

  @ViewModelInject
  public RegisterDeviceViewModel(RegisterDeviceUseCase registerDeviceUseCase) {
    this.registerDeviceUseCase = registerDeviceUseCase;
    this.registerDeviceUseCase.registerListener(this);
  }

  public void registerDevice() {
    registerDeviceUseCase.registerDevice();
  }

  public LiveData<Resource> getDeviceRegistrationStatus() {
    return deviceRegistrationStatus;
  }

  @Override
  public void onRegisterDeviceStarted(Resource<Boolean> result) {
    deviceRegistrationStatus.postValue(result);
  }

  @Override
  public void onRegisterDeviceSuccess(Resource success) {
    deviceRegistrationStatus.postValue(success);
  }

  @Override
  public void onRegisterDeviceFailed(Resource error) {
    deviceRegistrationStatus.postValue(error);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.registerDeviceUseCase.unregisterListener(this);
  }
}
