package id.co.danwinciptaniaga.androcon.auth;

import java.util.List;

import javax.inject.Inject;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.ApiResponse;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetUserPermissionUseCase
    extends BaseObservable<GetUserPermissionUseCase.Listener, List<PermissionInfo>> {
  public interface Listener {
    void onGetUserPermissionStarted();

    void onGetUserPermissionSuccess(Resource<List<PermissionInfo>> result);

    void onGetUserPermissionFailure(Resource<List<PermissionInfo>> result);
  }

  private final UserService userService;

  @Inject
  public GetUserPermissionUseCase(Application application, UserService userService) {
    super(application);
    this.userService = userService;
  }

  public void getUserPermission() {
    notifyStart(null, null);
    Call<List<PermissionInfo>> userPermissionsCall = this.userService.getUserPermissionsCall();
    userPermissionsCall.enqueue(new Callback<List<PermissionInfo>>() {
      @Override
      public void onResponse(Call<List<PermissionInfo>> call,
          Response<List<PermissionInfo>> response) {
        ApiResponse<List<PermissionInfo>> apiResponse = ApiResponse.create(response);
        if (apiResponse instanceof ApiResponse.ApiSuccessResponse) {
          List<PermissionInfo> permissionInfoList = ((ApiResponse.ApiSuccessResponse<List<PermissionInfo>>) apiResponse).getData();
          notifySuccess(null, permissionInfoList);
        } else if (apiResponse instanceof ApiResponse.ApiErrorResponse) {
          notifyFailure(null, ((ApiResponse.ApiErrorResponse) apiResponse).getErrorResponse(),
              null);
        }
      }

      @Override
      public void onFailure(Call<List<PermissionInfo>> call, Throwable t) {
        notifyFailure(null, null, t);
      }
    });
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<List<PermissionInfo>> result) {
    listener.onGetUserPermissionStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<List<PermissionInfo>> result) {
    listener.onGetUserPermissionSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<List<PermissionInfo>> result) {
    listener.onGetUserPermissionFailure(result);
  }
}
