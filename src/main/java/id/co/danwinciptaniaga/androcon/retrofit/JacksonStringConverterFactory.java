package id.co.danwinciptaniaga.androcon.retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import retrofit2.Converter;
import retrofit2.Retrofit;

public class JacksonStringConverterFactory extends Converter.Factory {
  private final transient ObjectMapper objectMapper;

  public JacksonStringConverterFactory(final ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public Converter<?, String> stringConverter(final Type type, final Annotation[] annotations,
      final Retrofit retrofit) {
    return new JacksonStringConverter(objectMapper.writer());
  }

  private static class JacksonStringConverter<T>
      implements Converter<T, String> {

    private final ObjectWriter adapter;

    JacksonStringConverter(ObjectWriter adapter) {
      this.adapter = adapter;
    }

    @Override
    public String convert(final T value) throws IOException {
      String result = adapter.writeValueAsString(value);
      if (result.startsWith("\"") && result.endsWith("\"")) {
        /* Strip enclosing quotes for json String types */
        return result.substring(1, result.length() - 1);
      } else {
        return result;
      }
    }
  }
}
